//
//  SearchGroupViewController.swift
//  puretennis
//
//  Created by IYE Technologies on 11/27/14.
//  Copyright (c) 2014 IYE Technologies. All rights reserved.
//

import UIKit

class SearchGroupViewController: UIViewController{
    @IBOutlet weak var resultsTableView: UITableView!
    
    var resultsTableDelegateAndDataSource:SearchResultsTableDelegateAndDataSource!;
    var searchBox:SearchBox!;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ViewUtils.addBackgroundView(self);
        
        self.addSearchBox();
        self.resultsTableDelegateAndDataSource = SearchResultsTableDelegateAndDataSource(searchBox: self.searchBox,navigationController: self.navigationController!);
        self.resultsTableView.dataSource = self.resultsTableDelegateAndDataSource;
        self.resultsTableView.delegate = self.resultsTableDelegateAndDataSource;
        
        self.initNavigationBar();
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated);
        self.hideTabBar();
    }
    
    func addSearchBox(){
        let searchBox = SearchBox(searchCallback: searchGroup,placeholder: "查找群");
        self.searchBox = searchBox;
        self.view.addSubview(self.searchBox);
    }
    
    func searchGroup(keywords:String){
        self.processing();
        let url = "/api/v1/group/search";
        let search = NSMutableDictionary();
        search.setObject(keywords, forKey: "keyword");
        
        HttpUtils.sendRequest(url, method: RequestMethod.POST, httpBody: search, successHandle: { (response) -> Void in
            var searchResults = response as NSMutableDictionary;
            var groups = searchResults.objectForKey("groups") as NSMutableArray;
            if(groups.count == 0){
                self.searchBox.notFound("没找到相关的群");
            }
            self.resultsTableDelegateAndDataSource!.setResults(groups);
            self.resultsTableView.reloadData();
            self.processComplete();
            }, errorHandle: { (error) -> Void in
                self.processComplete();
        });
    }
    
    func initNavigationBar(){
        self.navigationItem.titleView = ViewUtils.titleView("加入群");
        
        let backButton = UIButton();
        backButton.setImage(UIImage(named: "back"), forState: UIControlState.Normal);
        backButton.showsTouchWhenHighlighted = true;
        backButton.contentMode = UIViewContentMode.Left;
        backButton.sizeToFit();
        backButton.addTarget(self, action: Selector("back:"), forControlEvents: UIControlEvents.TouchUpInside);
        let backNavItem = UIBarButtonItem(customView: backButton);
        self.navigationItem.leftBarButtonItem = backNavItem;
        
        let createGroupButton = UIButton();
        createGroupButton.setImage(UIImage(named: "add"), forState: UIControlState.Normal);
        createGroupButton.showsTouchWhenHighlighted = true;
        createGroupButton.sizeToFit();
        createGroupButton.contentMode = UIViewContentMode.Right;
        createGroupButton.addTarget(self, action: Selector("createGroup:"), forControlEvents: UIControlEvents.TouchUpInside);
        let createGroupNavItem = UIBarButtonItem(customView: createGroupButton);
        self.navigationItem.rightBarButtonItem = createGroupNavItem;
    }
    
    func back(sender:UIButton){
        self.navigationController?.popViewControllerAnimated(true);
    }
    
    func createGroup(sender:UIButton){
        let storyboard = UIStoryboard(name: "Main", bundle: nil);
        let createGroupView = storyboard.instantiateViewControllerWithIdentifier("createOrUpdateGroupView") as CreateOrUpdateGroupViewController;
        createGroupView.create();
        self.navigationController?.pushViewController(createGroupView, animated: true);
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Utils.debug("Memory warning")
    }
}
