//
//  AbstractPickerView.swift
//  puretennis
//
//  Created by IYE Technologies on 12/25/14.
//  Copyright (c) 2014 IYE Technologies. All rights reserved.
//

import UIKit

class AbstractPickerView: UIControl,UIPickerViewDataSource,UIPickerViewDelegate {
    
    var margin:CGFloat = 10;
    var backgroud = UIImageView();
    var contentView = UIView();
    var confirmButton = UIButton();
    var cancelButton = UIButton();
    var showFrame:CGRect!;
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
    }
    
    override init(frame: CGRect) {
        self.showFrame = frame;
        
        super.init(frame: frame);
        self.frame.origin.y = self.showFrame.origin.y + self.showFrame.height;
        self.hidden = true;
//        self.backgroud.image = UIImage(named: "textfield_large");
//        self.backgroud.frame.size = frame.size;
//        self.addSubview(self.backgroud);
        self.backgroundColor = UIColor.whiteColor();
        self.layer.masksToBounds = true;
        self.layer.cornerRadius = 8;
        
        self.contentView.frame.size = self.showFrame.size;
        self.addSubview(contentView);
        
        self.cancelButton.setBackgroundImage(UIImage(named: "close"), forState: UIControlState.Normal);
        self.cancelButton.showsTouchWhenHighlighted = true;
        self.cancelButton.addTarget(self, action: Selector("cancel:"), forControlEvents: UIControlEvents.TouchUpInside);
        self.cancelButton.sizeToFit();
        self.cancelButton.frame.origin = CGPoint(x: margin, y: margin);
        self.addSubview(self.cancelButton);
        
        self.confirmButton.setBackgroundImage(UIImage(named: "confirm"), forState: UIControlState.Normal);
        self.confirmButton.showsTouchWhenHighlighted = true;
        self.confirmButton.sizeToFit();
        var originX = self.showFrame.width - self.confirmButton.frame.width - margin;
        self.confirmButton.frame.origin = CGPoint(x: originX, y: margin);
        self.confirmButton.addTarget(self, action: Selector("confirm:"), forControlEvents: UIControlEvents.TouchUpInside);
        self.addSubview(self.confirmButton);
    }
    
    // if don't use default method can be override in sub class
    func cancel(sender:UIButton){
        self.hide();
    }
    
    // if don't use default method can be override in sub class
    func confirm(sender:UIButton){
        sendActionsForControlEvents(UIControlEvents.ValueChanged);
        self.hide();
    }
    
    func show(){
        self.hidden = false;
        UIView.animateWithDuration(0.3, animations: { () -> Void in
            self.frame = self.showFrame;
        });
        UIView.commitAnimations();
    }
    
    
    func hide(){
        UIView.animateWithDuration(0.3, animations: { () -> Void in
            self.frame.origin.y = self.frame.origin.y + self.frame.height;
            }) { (complete:Bool) -> Void in
                self.hidden = true;
        }
        UIView.commitAnimations();
        
    }
    
    // If use picker delegate should be override this method
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int{
        fatalError("numberOfComponentsInPickerView(pickerView: UIPickerView) has not been implemented");
    }
    
    // If use picker delegate should be override this method
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        fatalError("pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) has not been implemented");
    }
}
