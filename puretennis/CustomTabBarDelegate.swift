//
//  CustomTabBarDelegate.swift
//  puretennis
//
//  Created by IYE Technologies on 12/1/14.
//  Copyright (c) 2014 IYE Technologies. All rights reserved.
//

import Foundation
protocol CustomTabBarDelegate:NSObjectProtocol{
    func hideCustomTabBar();
    
    
    func showCustomTabBar();
    
    
    func setBadge(value:Int);
}