//
//  GenderButton.swift
//  puretennis
//
//  Created by IYE Technologies on 11/21/14.
//  Copyright (c) 2014 IYE Technologies. All rights reserved.
//

import UIKit

class GenderButton: UIButton {

    var checked:Bool = false;
    
    func toggleCheckStatus(){
        var image:UIImage?;
        if(!checked){
            self.setImage(UIImage(named: "gender_checked"), forState: UIControlState.Normal);
            checked = true;
        }
    }
    
    func unchecked(){
        self.checked = false;
        self.setImage(UIImage(named: "gender_unchecked"), forState: UIControlState.Normal);
    }
    
    func check(){
        self.checked = true;
        self.setImage(UIImage(named: "gender_checked"), forState: UIControlState.Normal);
    }
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
