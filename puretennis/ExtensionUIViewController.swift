//
//  ExtensionUIViewController.swift
//  puretennis
//
//  Created by IYE Technologies on 12/1/14.
//  Copyright (c) 2014 IYE Technologies. All rights reserved.
//

import UIKit
extension UIViewController:UITextFieldDelegate{

    func hideTabBar(){
        var tabBarController:UITabBarController? = self.tabBarController;
        if(tabBarController != nil){
            var pureTennisTabBarController = tabBarController as PureTennisTabBarController;
            pureTennisTabBarController.tabBarView.hidden = true;
        }
    }
    
    
    func showTabBar(){
        var tabBarController:UITabBarController? = self.tabBarController;
        if(tabBarController != nil){
            var pureTennisTabBarController = tabBarController as PureTennisTabBarController;
            pureTennisTabBarController.tabBarView.hidden = false;
        }
    }
    
    public override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        super.touchesBegan(touches, withEvent: event);
        UIApplication.sharedApplication().sendAction(Selector("resignFirstResponder"), to: nil, from: nil, forEvent: nil);
    }
    
    func processing(){
//        GlobalVariables.timer = NSDate()
//        var viewSize = self.view.frame.size;
//        var loadingView = LoadingView(superViewSize: viewSize);
//        self.view.addSubview(loadingView);
    }
    
    func processComplete(){
//        if(GlobalVariables.timer != nil){
//            while(DateUtils.timeInterval(GlobalVariables.timer!, to: NSDate()) < 500){
//            }
//        }
//        var subviews = self.view.subviews
//        for subview in subviews{
//            if (subview is LoadingView){
//                subview.removeFromSuperview();
//            }
//        }
        
    }
    
    func navBarHeight() -> CGFloat?{
       return self.navigationController?.navigationBar.frame.height;
    }
    
    func tabBarHeight() -> CGFloat?{
       var pureTennisTabBarController = self.tabBarController! as PureTennisTabBarController;
        
       return pureTennisTabBarController.tabBarHeight;
    }
    
    func updateTabBarBadge(){
        var pureTennisTabBarController = self.tabBarController as PureTennisTabBarController;
        pureTennisTabBarController.updateBadge();
    }
}