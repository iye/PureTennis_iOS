//
//  ConfirmView.swift
//  puretennis
//
//  Created by IYE Technologies on 1/15/15.
//  Copyright (c) 2015 IYE Technologies. All rights reserved.
//

import UIKit

class ConfirmView:UIAlertView, UIAlertViewDelegate {
    var alertView:UIAlertView!;
    var confirm:(()->Void)?;
    var cancel:(()->Void)?;
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
    }
    
    init(message:String,confirm:(()->Void)?,cancel:(()->Void)?) {
        super.init();
        self.title = "";
        self.message = message;
        self.delegate = self;
        self.addButtonWithTitle("取消");
        self.addButtonWithTitle("确定");
        self.confirm = confirm;
        self.cancel = cancel;
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame);
    }

    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if(buttonIndex == 0){
            self.cancel?();
        } else {
            self.confirm?();
        }
    }
}
