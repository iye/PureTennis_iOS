//
//  CustomBadgeView.swift
//  puretennis
//
//  Created by IYE Technologies on 11/27/14.
//  Copyright (c) 2014 IYE Technologies. All rights reserved.
//

import UIKit

class CustomBadgeView: UIView {
    let huawenxingkai = UIFont(name: "STXingkai", size: 12);
    
    var badgeLabel:UILabel?;
    
    var backgroundImageView:UIImageView?;
    
    class func create(parentFrame:CGRect) ->CustomBadgeView{
        var badgeView = CustomBadgeView();
        badgeView.initialize(parentFrame);
        return badgeView;
    }
    
    func initialize(parentFrame:CGRect){
        var backgroundImageView = UIImageView(image: UIImage(named: "badge_bg"));
        backgroundImageView.sizeToFit();
        self.addSubview(backgroundImageView);
        self.frame.origin = CGPoint(x: parentFrame.width - backgroundImageView.frame.width, y: 0);
        self.badgeLabel = UILabel(frame: CGRect(x: 0, y: 0, width: backgroundImageView.frame.width , height: backgroundImageView.frame.height));
        self.badgeLabel!.textColor = UIColor.whiteColor();
        self.badgeLabel!.font = Utils.defaultFont()
        self.badgeLabel!.textAlignment = NSTextAlignment.Center;
        self.badgeLabel!.backgroundColor = UIColor.clearColor();
        self.addSubview(self.badgeLabel!);
    }
    
    func setBackgroupdImage(image:UIImage){
        let width = self.frame.width;
        let height = self.frame.height;
        let image = ImageUtils.RBSquareImageTo(image, size: CGSize(width: width, height: height));
        self.backgroundImageView?.image = image;
    }
    
    func setBadge(badge:Int){
        self.badgeLabel?.text = "\(badge)";
    }
}
