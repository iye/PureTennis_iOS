//
//  AppointmentRequestCell.swift
//  puretennis
//
//  Created by IYE Technologies on 1/4/15.
//  Copyright (c) 2015 IYE Technologies. All rights reserved.
//

import UIKit

class AppointmentRequestCell: AbstractPersionalCell {
    var acceptButton:UIButton!;
    
    var indexPath:NSIndexPath!
    
    var request:NSMutableDictionary!;
    
    class func create(indexPath:NSIndexPath,width:CGFloat,request:NSMutableDictionary) -> AppointmentRequestCell{
        
        var cell = AppointmentRequestCell();
        cell.request = request;
        cell.setWidth(width);
        var canOperation = request.objectForKey("canOperation") as Bool;
        if(canOperation){
            cell.setArrow("question");
        } else {
            cell.setArrow("right_arrow_mini");
        }
        var sender = request.objectForKey("sender") as NSDictionary;
        var senderGuid = sender.objectForKey("guid") as String;
        var avatar = ImageUtils.retrieveUserImage(senderGuid);
        cell.setAvatarImage(avatar);
        
        var name = sender.objectForKey("name") as String;
        cell.setName(name);
        return cell;
    }
 
}
