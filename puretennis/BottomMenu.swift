//
//  BottomMenu.swift
//  puretennis
//
//  Created by IYE Technologies on 1/19/15.
//  Copyright (c) 2015 IYE Technologies. All rights reserved.
//

import UIKit

class BottomMenu: UIView,UIActionSheetDelegate {
    
    var confirmButton:UIButton!;
    
    var trashButton:UIButton!;
    
    var deleteCallback:(()->Void)?;
    
    var confirmCallback:(()->Void)?;
    
    var normalFrame:CGRect!;
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
    }
    
    override init() {
        super.init();
        var bounds = UIScreen.mainScreen().bounds;
        var width = bounds.width;
        var height:CGFloat = 44;
        var originY = bounds.height - height;
        self.normalFrame = CGRect(x: 0, y: originY, width: width, height: height);
        
        var frame = CGRect(x: 0, y: bounds.height, width: width, height: height);
        self.frame = frame;
        self.backgroundColor = UIColor.whiteColor();
        self.layer.borderColor = UIColor.grayColor().CGColor;
        self.layer.borderWidth = 1;
        
        var confirmFrame = CGRect(x: 0, y: 0, width: self.frame.height, height: self.frame.height);
        self.confirmButton = UIButton(frame: confirmFrame);
        self.confirmButton.setImage(UIImage(named: "confirm"), forState: UIControlState.Normal);
        self.confirmButton.addTarget(self, action: Selector("confirmButtonTouchUpEnvent:"), forControlEvents: UIControlEvents.TouchUpInside);
        self.addSubview(confirmButton);
        
        
        var trashHeight = self.frame.height * 0.6;
        var trashY = (self.frame.height - trashHeight) / 2;
        var trashFrame = CGRect(x: self.frame.width - self.frame.height, y: trashY, width: trashHeight, height: trashHeight);
        self.trashButton = UIButton(frame: trashFrame);
        self.trashButton.setImage(UIImage(named: "trash"), forState: UIControlState.Normal);
        self.trashButton.addTarget(self, action: Selector("trashButtonTouchUpEnvent:"), forControlEvents: UIControlEvents.TouchUpInside);
        self.addSubview(self.trashButton);
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame);
    }
    
    func addConfirmCallback(callback:(()->Void)?){
        self.confirmCallback = callback;
    }
    
    func addDeleteCallback(callback:(()->Void)?){
        self.deleteCallback = callback;
    }
    
    func confirmButtonTouchUpEnvent(sender:UIButton){
        self.confirmCallback?();
    }
    
    func trashButtonTouchUpEnvent(sender:UIButton){
        var actionSheet = UIActionSheet();
        actionSheet.addButtonWithTitle("删除");
        actionSheet.addButtonWithTitle("取消");
        actionSheet.delegate = self;
        actionSheet.showInView(self.superview);
    }
    
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int) {
        if(buttonIndex == 0){
            self.deleteCallback?();
        }
    }
    
    func show(){
        UIView.animateWithDuration(0.3, animations: { () -> Void in
            self.frame = self.normalFrame;
        });
    }
}
