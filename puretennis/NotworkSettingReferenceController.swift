//
//  NotworkSettingReferenceController.swift
//  puretennis
//
//  Created by IYE Technologies on 1/9/15.
//  Copyright (c) 2015 IYE Technologies. All rights reserved.
//

import UIKit

class NotworkSettingReferenceController: UIViewController {

    @IBOutlet weak var textView1: UITextView!
    @IBOutlet weak var textView2: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad();
        ViewUtils.addBackgroundView(self);
        textView1.font = Utils.fontHupo(14);
        textView2.font = Utils.fontHupo(14);
        self.initNavigation();
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated);
        self.hideTabBar();
    }
    
    func initNavigation(){
        self.navigationItem.titleView = ViewUtils.titleView("网络无法连接");
        var backButton = UIButton();
        backButton.setImage(UIImage(named: "back"), forState: UIControlState.Normal);
        backButton.addTarget(self, action: Selector("back:"), forControlEvents: UIControlEvents.TouchUpInside);
        backButton.showsTouchWhenHighlighted = true;
        backButton.contentMode = UIViewContentMode.Left;
        backButton.sizeToFit();
        var backBarButtonItem = UIBarButtonItem(customView: backButton);
        self.navigationItem.leftBarButtonItem = backBarButtonItem;
    }
    
    func back(barButtonItem:UIBarButtonItem){
        self.navigationController?.popViewControllerAnimated(true);
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
