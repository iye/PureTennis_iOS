//
//  AbstractPersionalCell.swift
//  puretennis
//
//  Created by IYE Technologies on 12/31/14.
//  Copyright (c) 2014 IYE Technologies. All rights reserved.
//

import UIKit

class AbstractPersionalCell: UITableViewCell {
   
    var avatarImageView: UIImageView!
    
    var nameLabel: UILabel!
    
    var arrow = UIImageView();
    
    func setWidth(width:CGFloat){
        self.frame.size.width = width;
        self.contentView.frame.size.width = width;
        
        var backgroundImageView = UIImageView(image: UIImage(named: "cell_bg"));
        self.backgroundView = backgroundImageView;
        self.selectionStyle = UITableViewCellSelectionStyle.None;
        self.backgroundColor = UIColor.clearColor();
    }
 
    func setAvatarImage(avatarImage: UIImage){
        let height = self.contentView.frame.height
        let size = CGSize(width: height * 0.85, height: height * 0.85);
        self.avatarImageView = UIImageView();
        self.avatarImageView.frame.size = size;
        
        let originX:CGFloat = 5;
        let originY:CGFloat = (height - size.height - 4) / 2;
        let origin = CGPoint(x: originX, y: originY);
        self.avatarImageView.frame.origin = origin;
        
        self.avatarImageView.layer.masksToBounds = true;
        self.avatarImageView.layer.cornerRadius = self.avatarImageView.frame.height * 0.5;
        self.avatarImageView.image = avatarImage;
        self.contentView.addSubview(self.avatarImageView);

    }
    
    func setAvatar(avatarBase64:String){
        var avatarImage:UIImage!;
        
        if(avatarBase64.isEmpty){
            avatarImage = UIImage(named: "avatar");
        } else {
            avatarImage = ImageUtils.fromBase64String(avatarBase64);
        }
        
        self.setAvatarImage(avatarImage)
    }
    
    func setName(text:String){
        self.nameLabel = UILabel();
        let height = self.contentView.frame.height
        
        self.nameLabel.textAlignment = NSTextAlignment.Center;
        self.nameLabel.font = Utils.defaultFont();
        self.nameLabel.textColor = UIColor.whiteColor();
        self.nameLabel.text = text;
        self.nameLabel.sizeToFit();
        
        let originX = self.avatarImageView.frame.origin.x + self.avatarImageView.frame.width + 5;
        let originY = (self.contentView.frame.height - self.nameLabel.frame.height - 4) / 2;
        self.nameLabel.frame.origin = CGPoint(x: originX, y: originY);
        self.contentView.addSubview(self.nameLabel);
    }
    
    func setNameColor(color:UIColor){
        self.nameLabel.textColor = color;
    }
    
    func setArrow(named:String){
        self.arrow = UIImageView();
        self.arrow.contentMode = UIViewContentMode.Center;
        self.arrow.image = UIImage(named: named);
        self.arrow.sizeToFit();
        
        var originX = (self.frame.width - self.arrow.frame.width);
        var originY = (self.contentView.frame.height - self.arrow.frame.height - 4) / 2;
        self.arrow.frame.origin = CGPoint(x: originX, y: originY);
        self.contentView.addSubview(self.arrow);
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
