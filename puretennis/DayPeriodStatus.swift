//
//  DayPeriodStatus.swift
//  puretennis
//
//  Created by Derek Zheng on 12/2/14.
//  Copyright (c) 2014 IYE Technologies. All rights reserved.
//

import Foundation

enum DayPeriodStatus: String{
    case YES = "YES"
    case NO = "NO"
    case SCHEDULED = "SCHEDULED"
    
    static func toStatus(text: String) -> DayPeriodStatus{
        return DayPeriodStatus(rawValue: text)!;
    }
}