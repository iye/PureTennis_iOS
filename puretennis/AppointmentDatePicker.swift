//
//  AppointmentDatePicker.swift
//  puretennis
//
//  Created by IYE Technologies on 1/6/15.
//  Copyright (c) 2015 IYE Technologies. All rights reserved.
//

import UIKit

class AppointmentDatePicker: UIPickerView,UIPickerViewDataSource,UIPickerViewDelegate {
    
    var pickerView = UIPickerView();
    
    let plusMonts:NSArray = [1,3,5,7,8,10,12];
    var years:[Int] = [];
    var months:[Int] = [];
    var days:[Int] = [];
    
    var year:Int!;
    var month:Int!;
    var day:Int!;
    
    var date = NSDate();
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
    }
    
    override init() {
        super.init();
        self.dataSource = self;
        self.delegate = self;
        self.initYears();
        self.initMonths();
        self.initDays();
        self.defaultSelect();
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame);
    }
    
    func initYears(){
        var currentYear = getCurrentYear();
        self.year = currentYear;
        for(var year = currentYear; year < currentYear + 50; year++){
            self.years.append(year);
        }
    }
    
    func initMonths(){
        self.month = getCurrentMonth();
        self.months = [1,2,3,4,5,6,7,8,9,10,11,12];
        
    }
    
    func initDays(){
        self.day = getCurrentDay();
        for(var day = 1;day <= 31; day++){
            self.days.append(day);
        }
    }
    
    func defaultSelect(){
        self.month = getCurrentMonth();
        self.selectRow(self.month - 1, inComponent: 1, animated: false);
        
        self.day = getCurrentDay();
        self.selectRow(self.day - 1, inComponent: 2, animated: false);
        self.reloadAllComponents();
    }
    
    func getCurrentYear() -> Int{
        var now = NSDate();
        var yearAsString = DateUtils.toString(now, format: "yyyy");
        return yearAsString.toInt()!;
    }
    
    func getCurrentMonth() -> Int{
        var now = NSDate();
        var monthAsString = DateUtils.toString(now, format: "MM");
        return monthAsString.toInt()!;
    }
    
    func getCurrentDay() -> Int{
        var now = NSDate();
        var dayAsString = DateUtils.toString(now, format: "dd");
        return dayAsString.toInt()!;
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int{
        return 3;
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        switch component{
        case 0:
            return years.count;
        case 1:
            return months.count;
        default:
            return days.count;
        }
    }
    
    func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView!) -> UIView {
        var label = UILabel();
        if(component == 0){
            var year = self.years[row];
            label.text = "\(year)年";
            label.sizeToFit();
            return label;
        }
        
        if(component == 1){
            var month = self.months[row];
            var currentMonth = getCurrentMonth();
            if(month < currentMonth){
                label.textColor = UIColor.lightGrayColor();
            }
            label.text = "\(month)月";
            label.sizeToFit();
            return label;
        }
        var currentYear = getCurrentYear();
        var currentMonth = getCurrentMonth();
        var currentDay = getCurrentDay();
        var day = self.days[row];
        if(day < currentDay && self.month <= currentMonth && self.year <= currentYear){
            label.textColor = UIColor.lightGrayColor();
        }
        
        var leapYear = ((self.year % 4) == 0);
        if(!leapYear && self.month == 2 && day > 28){
            label.textColor = UIColor.lightGrayColor();
        }
        
        if(leapYear && self.month == 2 && day > 29){
            label.textColor = UIColor.lightGrayColor();
        }
        
        if(!self.plusMonts.containsObject(self.month) && day > 30){
            label.textColor = UIColor.lightGrayColor();
        }
        
        label.text = "\(day)日";
        label.sizeToFit();
        return label;
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if(component == 0){
            self.year = self.years[row];
        }
        
        if(component == 1){
            self.month = self.months[row];
        }
        
        if(component == 2){
            self.day = self.days[row];
        }
        pickerView.reloadComponent(2);
        
        var currentYear = getCurrentYear();
        var currentMonth = getCurrentMonth();
        if(self.month < currentMonth){
            self.month = currentMonth;
            pickerView.selectRow(self.month - 1, inComponent: 1, animated: true);
        }
        
        var currentDay = getCurrentDay();
        if(self.day < currentDay && self.month <= currentMonth && self.year <= currentYear){
            self.day = currentDay;
            pickerView.selectRow(self.day - 1, inComponent: 2, animated: true);
        }
        
        var leapYear = ((self.year % 4) == 0);
        if(!leapYear && self.month == 2 && self.day > 28){
            self.day = 28;
            pickerView.selectRow(27, inComponent: 2, animated: true);
        }
        
        if(leapYear && self.month == 2 && self.day > 29){
            self.day = 29;
            pickerView.selectRow(28, inComponent: 2, animated: true);
        }
        
        if(!self.plusMonts.containsObject(self.month) && self.day > 30){
            self.day = 30;
            pickerView.selectRow(29, inComponent: 2, animated: true);
        }
        
        self.date = DateUtils.parseDate("\(self.year)-\(self.month)-\(self.day)", format: "yyyy-MM-dd");
    }
    
    func pickerView(pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 35;
    }

}
