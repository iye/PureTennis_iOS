//
//  GroupCell.swift
//  puretennis
//
//  Created by IYE Technologies on 12/24/14.
//  Copyright (c) 2014 IYE Technologies. All rights reserved.
//

import UIKit

class GroupCell: UITableViewCell {
    private var margin:CGFloat = 5;
    
    private var groupNameLabel = UILabel();
    
    private var detailsLabel = UILabel();
    
    private var badgeButton:UIButton?;
    
    class func create(width:CGFloat,group:NSDictionary) -> GroupCell{
        var cell = GroupCell();
        cell.setWidth(width);
        var groupName = group.objectForKey("groupName") as String;
        cell.setGroupName(groupName);
        var participantsAmount = group.objectForKey("participantsAmount") as Int;
        cell.setDetails("\(participantsAmount)人");
        var isOwner = group.objectForKey("owner") as Bool;
        if(isOwner){
            var requestAmount = group.objectForKey("requestAmount") as Int;
            var unreadMessageAmount = group.objectForKey("unreadMessageAmount") as Int;
            cell.setBadge(requestAmount + unreadMessageAmount);
        }
        return cell;
    }
    
    func setWidth(width:CGFloat){
        self.frame.size.width = width;
        self.contentView.frame.size.width = width;
        
        var backgroundImageView = UIImageView(image: UIImage(named: "cell_bg"));
        self.backgroundView = backgroundImageView;
        self.selectionStyle = UITableViewCellSelectionStyle.None;
        self.backgroundColor = UIColor.clearColor();
    }
    
    func setGroupName(groupName:String){
        self.groupNameLabel.font = Utils.defaultFont();
        self.groupNameLabel.textColor = UIColor.whiteColor();
        self.groupNameLabel.text = groupName;
        self.groupNameLabel.sizeToFit();
        var originY = (self.contentView.frame.height - self.groupNameLabel.frame.height - 4) / 2;
        self.groupNameLabel.frame.origin = CGPoint(x: margin, y: originY);
        self.contentView.addSubview(self.groupNameLabel);
    }
    
    func setDetails(text:String){
        self.detailsLabel.font = Utils.defaultFont();
        self.detailsLabel.textColor = UIColor.whiteColor();
        self.detailsLabel.text = text;
        self.detailsLabel.sizeToFit();
        var originX = self.contentView.frame.width - self.detailsLabel.frame.width - margin;
        var originY = (self.contentView.frame.height - self.detailsLabel.frame.height - 4) / 2;
        self.detailsLabel.frame.origin = CGPoint(x: originX, y: originY);
        self.contentView.addSubview(self.detailsLabel);
    }
    
    func adjustDetailsPosition(){
        var originX = self.badgeButton!.frame.origin.x - self.detailsLabel.frame.width - margin;
        self.detailsLabel.frame.origin.x = originX;
    }
    
    func setBadge(badge:Int){
        if(badge > 0){
            self.badgeButton = UIButton();
            self.badgeButton!.setTitle("\(badge)", forState: UIControlState.Normal);
            self.badgeButton!.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal);
            self.badgeButton!.contentMode = UIViewContentMode.Center;
            self.badgeButton!.titleLabel!.font = Utils.defaultFont();
            self.badgeButton!.titleLabel!.textAlignment = NSTextAlignment.Center;
            self.badgeButton!.setBackgroundImage(UIImage(named: "badge_bg"), forState: UIControlState.Normal);
            self.badgeButton!.sizeToFit();
            var originX = self.detailsLabel.frame.origin.x - self.badgeButton!.frame.width - margin;
            var originY = (self.contentView.frame.height - self.badgeButton!.frame.height - 4) / 2;
            self.badgeButton!.frame.origin = CGPoint(x: originX, y: originY);
            self.contentView.addSubview(self.badgeButton!);
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
