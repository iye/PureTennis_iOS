//
//  PureTextCell.swift
//  puretennis
//
//  Created by IYE Technologies on 12/10/14.
//  Copyright (c) 2014 IYE Technologies. All rights reserved.
//

import UIKit

class PureTextCell: UITableViewCell {

    var leftLabel:UILabel!;
    var rightlabel:UILabel!;
    var arrow:UIImageView!

    
    class func create(width:CGFloat,leftText:String,rightText:String) -> PureTextCell{
        var cell = PureTextCell();
        cell.setWidth(width);
        cell.setArrow("right_arrow_mini");
        cell.setLeftText(leftText);
        cell.setRightText(rightText);
        return cell;
    }
    

    func setWidth(width:CGFloat){
        self.frame.size.width = width;
        self.contentView.frame.size.width = width;
        
        var backgroundImageView = UIImageView(image: UIImage(named: "cell_bg"));
        self.backgroundView = backgroundImageView;
        self.selectionStyle = UITableViewCellSelectionStyle.None;
        self.backgroundColor = UIColor.clearColor();
    }

    func setLeftText(text:String){
        self.leftLabel = UILabel();
        let height = self.contentView.frame.height
        
        self.leftLabel.textAlignment = NSTextAlignment.Left;
        self.leftLabel.font = Utils.defaultFont();
        self.leftLabel.textColor = UIColor.whiteColor();
        self.leftLabel.text = text;
        self.leftLabel.sizeToFit();
        
        let originX:CGFloat = 5
        let originY = (self.contentView.frame.height - self.leftLabel.frame.height - 4) / 2;
        self.leftLabel.frame.origin = CGPoint(x: originX, y: originY);
        self.contentView.addSubview(self.leftLabel);
    }
    
    func setRightText(text:String){
        self.rightlabel = UILabel();
        self.rightlabel.text = text;
        self.rightlabel.textAlignment = NSTextAlignment.Right;
        self.rightlabel.font = Utils.defaultFont();
        self.rightlabel.textColor = UIColor.whiteColor();
        self.rightlabel.sizeToFit();
        
        var width = self.frame.width - self.leftLabel.frame.width - self.arrow.frame.height;
        var originX = self.leftLabel.frame.origin.x + self.leftLabel.frame.width + 5;
        
        var originY = (self.frame.height - self.leftLabel.frame.height - 4) / 2;
        
        self.rightlabel.frame.origin = CGPoint(x: originX, y: originY);
        self.rightlabel.frame.size.width = width;
        self.contentView.addSubview(self.rightlabel);
    }
    
    func setArrow(named:String){
        var height = self.frame.height;
        var arrowImage = UIImage(named: named);
        self.arrow = UIImageView();
        self.arrow.frame.size = CGSize(width: height, height: height);
        self.arrow.image = arrowImage;
        self.arrow.sizeToFit();
        var originX = (self.frame.width - self.arrow.frame.width);
        var originY = (self.contentView.frame.height - self.arrow.frame.height - 4) / 2;
        self.arrow.frame.origin = CGPoint(x: originX, y: originY);
        self.contentView.addSubview(self.arrow);
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    

}
