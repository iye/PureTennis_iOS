//
//  PeriodButton.swift
//  puretennis
//
//  Created by IYE Technologies on 12/8/14.
//  Copyright (c) 2014 IYE Technologies. All rights reserved.
//

import UIKit

class PeriodButton: UIButton {

    var period:String!;
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    func period(period:String){
        self.period = period;
    }
    
    func periodLowercaseString() -> String{
        return period.lowercaseString;
    }
    
    
}
