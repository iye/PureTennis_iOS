//
//  PureTennisNavigationController.swift
//  puretennis
//
//  Created by IYE Technologies on 12/23/14.
//  Copyright (c) 2014 IYE Technologies. All rights reserved.
//

import UIKit

class PureTennisNavigationController: UINavigationController,UINavigationControllerDelegate,UIGestureRecognizerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad();
        self.delegate = self;
        if(self.respondsToSelector(Selector("interactivePopGestureRecognizer"))){
            self.interactivePopGestureRecognizer.delegate = self;
            self.interactivePopGestureRecognizer.enabled = false;
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        ViewUtils.reloadBages()
    }
    
    func navigationController(navigationController: UINavigationController, didShowViewController viewController: UIViewController, animated: Bool) {
        if(viewController is GroupViewController || viewController is AppointmentAndAvailabilityViewController || viewController is MyAvailabilityViewController || viewController is MyDetailsViewController){
            self.interactivePopGestureRecognizer.enabled = false;
            return;
        }
        self.interactivePopGestureRecognizer.enabled = true;
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Utils.debug("Memory warning")
    }
}
