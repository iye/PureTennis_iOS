//
//  CustomTextView.swift
//  puretennis
//
//  Created by IYE Technologies on 1/7/15.
//  Copyright (c) 2015 IYE Technologies. All rights reserved.
//

import UIKit

class CustomTextView: UITextView {

    var placeholder:UILabel!;
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
        self.initPlaceholder();
        
    }
    override func willMoveToSuperview(newSuperview: UIView?) {
        super.willMoveToSuperview(newSuperview);
    }
    
    func initPlaceholder(){
        self.placeholder = UILabel();
        self.placeholder.frame.origin = CGPoint(x: 0, y: 0);
        self.placeholder.text = "备注";
        self.placeholder.sizeToFit();
        self.placeholder.font = Utils.defaultFont();
        self.placeholder.textColor = UIColor.whiteColor();
        self.placeholder.enabled = false;
        self.addSubview(self.placeholder);
    }

}
