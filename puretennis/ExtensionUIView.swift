//
//  ExtensionUIView.swift
//  puretennis
//
//  Created by IYE Technologies on 1/12/15.
//  Copyright (c) 2015 IYE Technologies. All rights reserved.
//

import UIKit

extension UIView{
    func shake(){
        var keyAnimation = CAKeyframeAnimation(keyPath: "position");
        var values = NSArray(objects:
            NSValue(CGPoint: CGPointMake(self.center.x, self.center.y)),
            NSValue(CGPoint: CGPointMake(self.center.x-5, self.center.y)),
            NSValue(CGPoint: CGPointMake(self.center.x+5, self.center.y)),
            NSValue(CGPoint: CGPointMake(self.center.x, self.center.y)),
            NSValue(CGPoint: CGPointMake(self.center.x-5, self.center.y)),
            NSValue(CGPoint: CGPointMake(self.center.x+5, self.center.y)),
            NSValue(CGPoint: CGPointMake(self.center.x, self.center.y)),
            NSValue(CGPoint: CGPointMake(self.center.x-5, self.center.y)),
            NSValue(CGPoint: CGPointMake(self.center.x+5, self.center.y)),
            NSValue(CGPoint: CGPointMake(self.center.x, self.center.y))
        );
        keyAnimation.values = values;
        var keyTimes = NSArray(objects: 0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0);
        keyAnimation.keyTimes = keyTimes;
        self.layer.addAnimation(keyAnimation, forKey: "ShakeAnimation");
    }
}
