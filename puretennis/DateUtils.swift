//
//  DateUtils.swift
//  puretennis
//
//  Created by IYE Technologies on 11/15/14.
//  Copyright (c) 2014 IYE Technologies. All rights reserved.
//

import Foundation
import UIKit

class DateUtils {
    
    let localTimeZone = NSTimeZone.localTimeZone();
    
    class func plusDays(date:NSDate,days:Int) -> NSDate{

        
        var calendar = NSCalendar(calendarIdentifier: NSGregorianCalendar)!
        
        let components = NSDateComponents();
        components.day = days;
     
        return calendar.dateByAddingComponents(components, toDate: date, options: nil)!;
    }
    
    class func plusWeeks(date:NSDate,weeks:Int) -> NSDate{
        var calendar = NSCalendar(calendarIdentifier: NSGregorianCalendar)!
        let components = NSDateComponents();
        
        components.weekOfYear = weeks;
        return calendar.dateByAddingComponents(components, toDate: date, options: nil)!;
    }
    
    
    class func parseDate(dateAsText:String,format:String) -> NSDate{
        let formatter  = NSDateFormatter();
        formatter.dateFormat = format;
        return formatter.dateFromString(dateAsText)!;
    }
    
    class func firstDayOfCurrentWeek() -> NSDate {
        var date = NSDate();
        var cal = NSCalendar.currentCalendar();
        var comp = cal.components(NSCalendarUnit.CalendarUnitWeekday, fromDate: date);
        var dayOfWeek = comp.weekday - 1;
        if(dayOfWeek == 1){
            return date
        }else{
            return DateUtils.plusDays(date, days: -(dayOfWeek - 1))
        }
    }
    
    class func now()->NSDate {
        return NSDate();
    }
    
    class func toString(date:NSDate,format:String)->String{
        let formatter  = NSDateFormatter();
        formatter.dateFormat = format;
        return formatter.stringFromDate(date);
    }
    
    class func timeDifferenceCalculator(gmtDate:NSDate) -> NSDate{
        let localTimeZone = NSTimeZone.localTimeZone();
        let interval = localTimeZone.secondsFromGMTForDate(gmtDate);
        return gmtDate.dateByAddingTimeInterval(NSTimeInterval(interval));
    }
    
    class func isToday(date: NSDate) -> Bool{
        var now = self.now();
        var nowAsText = self.toString(now, format: "yyyyMMdd");
        var dateAsText = self.toString(date, format: "yyyyMMdd");
        return nowAsText == dateAsText;
    }

    class func isTomorrow(date: NSDate) -> Bool{
        var tomorrow = DateUtils.plusDays(DateUtils.now(), days: 1)
        var nowAsText = self.toString(tomorrow, format: "yyyyMMdd");
        var dateAsText = self.toString(date, format: "yyyyMMdd");
        return nowAsText == dateAsText;
    }

    class func timeInterval(from:NSDate,to:NSDate) -> Int{
        let interval = to.timeIntervalSinceDate(from) * 1000;
        let decimalNumber = decimalHandler(interval, scale: 4);
        return decimalNumber.integerValue;
    }
    
    class func decimalHandler(number:Double,scale:Int16) ->NSDecimalNumber {
        let behaviors = NSDecimalNumberHandler(roundingMode: NSRoundingMode.RoundUp, scale: scale, raiseOnExactness: false, raiseOnOverflow: false, raiseOnUnderflow: false, raiseOnDivideByZero: false);
        return NSDecimalNumber(double: number).decimalNumberByRoundingAccordingToBehavior(behaviors);
    }
    
    class func isBefore(date1:NSDate,date2:NSDate) -> Bool{
        return DateUtils.timeInterval(date1, to: date2) > 0
    }
    
    class func isAfter(date1:NSDate,date2:NSDate) -> Bool{
        return DateUtils.timeInterval(date1, to: date2) < 0
    }
    
    class func isAfterToday(date:NSDate) -> Bool {
        var dateAsString = DateUtils.toString(date, format: "yyyyMMdd");
        var dateAsInt = dateAsString.toInt();
        var todayAsString = DateUtils.toString(NSDate(), format: "yyyyMMdd");
        var todayAsInt = todayAsString.toInt();
        return dateAsInt > todayAsInt;
    }
    
    class func isBeforeToday(date:NSDate) -> Bool{
        var dateAsString = DateUtils.toString(date, format: "yyyyMMdd");
        var dateAsInt = dateAsString.toInt();
        var todayAsString = DateUtils.toString(NSDate(), format: "yyyyMMdd");
        var todayAsInt = todayAsString.toInt();
        return dateAsInt < todayAsInt;
    }
    
    class func isEqual(date1:NSDate,date2:NSDate) -> Bool{
        return toString(date1, format: "yyyyMMdd") == toString(date2, format: "yyyyMMdd")
    }
    
    class func isDateInToday(dateAsString:String,fomat:String) -> Bool{
        var date = parseDate(dateAsString, format: fomat);
        return isToday(date)
    }
    
    class func isDateInTomorrow(dateAsString:String,fomat:String) -> Bool{
        var date = parseDate(dateAsString, format: fomat);
        var currentCalendar = NSCalendar.currentCalendar();
        return isTomorrow(date)
    }
    
    class func intervalToNowText(time: String) -> String{
        var date = DateUtils.parseDate(time, format: "yyyy-MM-dd HH:mm:ss")
        var now = DateUtils.now()
        var intervalMills = DateUtils.timeInterval(date, to: now)
        var intervalMinutes = intervalMills / 1000 / 60;
        if(intervalMinutes == 0){
            return "\(intervalMills / 1000)秒"
        }else if(intervalMinutes < 60){
            return "\(intervalMinutes)分钟"
        }else if((intervalMinutes / 60) < 24 ){
            return "\(intervalMinutes / 60)小时"
        }else {
            return "\(intervalMinutes / 60 / 24)天"
        }
    }
    
    
    class func dateResolve(date:NSDate) -> String{
        var currentCalendar = NSCalendar.currentCalendar();
        
        if(DateUtils.isToday(date)){
            return "今天";
        }
        
        if(DateUtils.isTomorrow(date)){
            return "明天";
        }
        

        if(DateUtils.isTomorrow(plusDays(date, days: -1))){
            return "后天";
        }
        
        var weekDate = DateUtils.toString(date, format: "EEEE");
        var monthDate = DateUtils.toString(date, format: "MM月dd日");
        
        return "\(monthDate),\(weekDate)";
    }
    
    class func getCurrentHour() -> Int{
        var now = NSDate();
        var currentHourAsString = DateUtils.toString(now, format: "HH");
        return currentHourAsString.toInt()!;
    }
    
    class func getCurrentMinute() -> Int{
        var now = NSDate();
        var currentMinuteAsString = DateUtils.toString(now, format: "mm");
        return currentMinuteAsString.toInt()!;
    }
    
}
