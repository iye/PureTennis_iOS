//
//  DayAvailabilityImageView.swift
//  puretennis
//
//  Created by Derek Zheng on 12/2/14.
//  Copyright (c) 2014 IYE Technologies. All rights reserved.
//

import UIKit

class DayAvailabilityImageView: UIImageView {
    
    var date: String?
    var dateText: String?
    var dayText: String?
    var morning: PeriodFrameImageView?
    var afternoon: PeriodFrameImageView?
    var evening: PeriodFrameImageView?
    var dateObj: NSDate?
    
    


    init(date: NSDate, position: CGPoint, width: CGFloat){
        super.init(image: UIImage(named: "frame"))
        self.dateObj = date
        self.date = DateUtils.toString(date, format: "yyyy-MM-dd");
        self.dateText = DateUtils.toString(date, format: "M月d日");
        self.dayText = DateUtils.toString(date, format:"EEEE");
        self.frame.origin = position
        
        let ratio = self.frame.size.height / self.frame.size.width
        
        let size = CGSize(width: width, height: width*ratio)
        
        self.frame.size = size
        
        self.morning = PeriodFrameImageView(day:self, status:DayPeriodStatus.NO, label: DayPeriod.MORNING);
        self.afternoon = PeriodFrameImageView(day:self, status:DayPeriodStatus.NO, label: DayPeriod.AFTERNOON);
        self.evening = PeriodFrameImageView(day:self, status:DayPeriodStatus.NO, label: DayPeriod.EVENING);
        self.addTextToFrame()
        
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func resolveAvailability(dateText: String, availabilities: NSMutableDictionary){
        if(date == dateText){
            updatePeriodStatus(availabilities.objectForKey("morning") as String, period: self.morning!);
            updatePeriodStatus(availabilities.objectForKey("afternoon") as String, period: self.afternoon!);
            updatePeriodStatus(availabilities.objectForKey("evening") as String, period: self.evening!);

        }
    }
    
    func updatePeriodStatus(statusAsText: String, period: PeriodFrameImageView){
        var status = DayPeriodStatus.toStatus(statusAsText);
        
        period.onStatusChange(status)
        
        
    }
    
    override func removeFromSuperview(){
        morning?.removeFromSuperview();
        morning = nil;
        afternoon?.removeFromSuperview();
        afternoon = nil;
        evening?.removeFromSuperview();
        evening = nil;
        
        super.removeFromSuperview();        
    }
    
    func addTextToFrame() {
        var color = UIColor.whiteColor();
        if(DateUtils.isToday(dateObj!)){
            color = UIColor.yellowColor()
        }

        var dateTextLabel: UILabel = UILabel(frame: CGRect(x:0, y:self.frame.height*0.2, width: self.frame.width, height: self.frame.height*0.3));
        dateTextLabel.text = dateText;
        dateTextLabel.textColor = color
        dateTextLabel.font = Utils.fontHupo(12);
        dateTextLabel.textAlignment = NSTextAlignment.Center

        self.addSubview(dateTextLabel);
        
        var dayTextLabel: UILabel = UILabel(frame:CGRect(x: 0, y: dateTextLabel.frame.height + self.frame.height*0.2, width: self.frame.width, height: self.frame.height*0.3));
        dayTextLabel.text = dayText
        dayTextLabel.font = Utils.fontHupo(10);
        dayTextLabel.textColor = color
        dayTextLabel.textAlignment = NSTextAlignment.Center
        self.addSubview(dayTextLabel)
        
    }
    
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
