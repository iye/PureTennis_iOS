//
//  EditMyInfoViewController.swift
//  puretennis
//
//  Created by IYE Technologies on 1/7/15.
//  Copyright (c) 2015 IYE Technologies. All rights reserved.
//

import UIKit

class EditMyInfoViewController: ImagePickerController {

    var adjustContentToAccommodateKeyboard:AdjustContentToAccommodateKeyboard!;

    @IBOutlet weak var avatarImageView:UIImageView!;
    @IBOutlet weak var nicknameTextField:CustomTextField!;
    @IBOutlet weak var femaleButton:GenderButton!;
    @IBOutlet weak var maleButton:GenderButton!;
  
    var gender = "MALE";
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ViewUtils.addBackgroundView(self);
        self.initNavigation();
        self.initAvatarImageView();
        self.loadMyDetails();
        self.adjustContentToAccommodateKeyboard = AdjustContentToAccommodateKeyboard(target: self);
    }
    
    func initNavigation(){
        var backButton = UIButton();
        backButton.setBackgroundImage(UIImage(named: "back"), forState: UIControlState.Normal);
        backButton.showsTouchWhenHighlighted = true;
        backButton.sizeToFit();
        backButton.addTarget(self, action: Selector("back:"), forControlEvents: UIControlEvents.TouchUpInside);
        var backBarItem = UIBarButtonItem(customView: backButton);
        self.navigationItem.leftBarButtonItem = backBarItem;
        
        var saveButton = UIButton();
        saveButton.setBackgroundImage(UIImage(named: "confirm"), forState: UIControlState.Normal);
        saveButton.showsTouchWhenHighlighted = true;
        saveButton.addTarget(self, action: Selector("saveMyDetails:"), forControlEvents: UIControlEvents.TouchUpInside);
        saveButton.sizeToFit();
        var saveBarItem = UIBarButtonItem(customView: saveButton);
        self.navigationItem.rightBarButtonItem = saveBarItem;
    }
    
    func saveMyDetails(sender:UIButton){
        if(validate()){
            self.processing();
            let url = "/api/v1/profile"
            var postDictionary = NSMutableDictionary();
            postDictionary.setObject(self.gender, forKey: "gender");
            
            let nickname = self.nicknameTextField.text;
            postDictionary.setObject(nickname, forKey: "nickname");
            
            HttpUtils.sendRequest(url, method: RequestMethod.POST, httpBody: postDictionary, successHandle: { (response) -> Void in
                var tabBarController = AuthorisationService.mainView();
                self.processComplete();
                self.navigationController?.popViewControllerAnimated(true);
                }, errorHandle: { (error) -> Void in
                   self.processComplete();
            });
        }
    }
    
    func validate() -> Bool{
        var nickname = self.nicknameTextField.text;
        if(nickname.isEmpty){
            self.nicknameTextField.shake();
        }
        return !nickname.isEmpty;
    }
    
    // Back
    func back(sender:UIButton){
        self.navigationController?.popViewControllerAnimated(true);
    }
    
    func initAvatarImageView(){
        self.avatarImageView.layer.masksToBounds = true;
        self.avatarImageView.layer.cornerRadius = 8;
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated);
        self.hideTabBar();
        
    }

    func loadMyDetails(){
        self.processing();
        var userGuid = Utils.currentUserGuid();
        self.avatarImageView.image = ImageUtils.retrieveUserImage(userGuid,reload: true);
        var url = "/api/v1/user/details";
        HttpUtils.sendRequest(url, method: RequestMethod.GET, httpBody: nil, successHandle: { (response) -> Void in
            var myDetails = response as NSDictionary;
            
            var nickname = myDetails.objectForKey("nickname") as String;
            self.nicknameTextField.text = nickname;
            
            self.gender = myDetails.objectForKey("gender") as String;
            if(self.gender == "FEMALE"){
                self.femaleButton.setImage(UIImage(named: "female_checked"), forState: UIControlState.Normal);
                self.maleButton.setImage(UIImage(named: "male_unchecked"), forState: UIControlState.Normal);
            } else {
                self.femaleButton.setImage(UIImage(named: "female_unchecked"), forState: UIControlState.Normal);
                self.maleButton.setImage(UIImage(named: "male_checked"), forState: UIControlState.Normal);
            }
            self.processComplete();
        }, errorHandle: { (error) -> Void in
            self.processComplete();
        });
    }
    
    @IBAction func femaleButtonTouchUp(sender: UIButton) {
        if(self.gender != "FEMALE"){
            self.gender = "FEMALE";
            sender.setImage(UIImage(named: "female_checked"), forState: UIControlState.Normal);
            self.maleButton.setImage(UIImage(named: "male_unchecked"), forState: UIControlState.Normal);
        }
    }
    
    @IBAction func maleButtonTouchUp(sender: UIButton) {
        if(self.gender != "MALE"){
            self.gender = "MALE";
            sender.setImage(UIImage(named: "male_checked"), forState: UIControlState.Normal);
            self.femaleButton.setImage(UIImage(named: "female_unchecked"), forState: UIControlState.Normal);
        }
    }
    
    @IBAction func choiceAvatAR(sender: UIButton) {
        self.pickingImage();
    }
    
    override func didFinishPickingImage(image: UIImage!) {
        self.avatarImageView.image = image;
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
