//
//  UserInforViewController.swift
//  puretennis
//
//  Created by IYE Technologies on 11/21/14.
//  Copyright (c) 2014 IYE Technologies. All rights reserved.
//

import UIKit

class UserInforViewController: ImagePickerController{

    @IBOutlet weak var avatarImageView:UIImageView!;
    @IBOutlet weak var nicknameTextField:CustomTextField!;
    @IBOutlet weak var femaleButton:GenderButton!;
    @IBOutlet weak var maleButton:GenderButton!;
    
    var gender = "MALE";

    var userInfo:UserInfo!;
    var adjustContentToAccommodateKeyboard:AdjustContentToAccommodateKeyboard!;
    override func viewDidLoad() {
        super.viewDidLoad()
        ViewUtils.addBackgroundView(self);
        self.initAvatarImageView();
        self.loadUserInfo();
        self.adjustContentToAccommodateKeyboard = AdjustContentToAccommodateKeyboard(target: self);
    }
    
    func initAvatarImageView(){
        self.avatarImageView.layer.masksToBounds = true;
        self.avatarImageView.layer.cornerRadius = 8;
    }
    
    func selecedIdol(idolPicker:IdolPicker){
        var idol = idolPicker.selecedIdol;
        var idolName = idol.objectForKey("name") as String;
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated);
    }
    
    func setUserInfo(userInfo:UserInfo){
        self.userInfo = userInfo;
    }
    
    // Load user info
    func loadUserInfo(){
        if(userInfo == nil){
            self.processing();
            let guid = Utils.getValue("guid") as String;
            let url = "/api/v1/profile/" + guid;
            HttpUtils.sendRequest(url, method: RequestMethod.GET, httpBody: nil, successHandle: { (response) -> Void in
                var userInfoDictionary = response as NSMutableDictionary;
                var nickname = userInfoDictionary.objectForKey("nickname") as String;
                self.nicknameTextField.text = nickname;
                self.gender = userInfoDictionary.objectForKey("gender") as String;
                if(self.gender == "FEMALE"){
                    self.femaleButton.setImage(UIImage(named: "female_checked"), forState: UIControlState.Normal);
                    self.maleButton.setImage(UIImage(named: "male_unchecked"), forState: UIControlState.Normal);
                } else {
                    self.femaleButton.setImage(UIImage(named: "female_unchecked"), forState: UIControlState.Normal);
                    self.maleButton.setImage(UIImage(named: "male_checked"), forState: UIControlState.Normal);
                }
                var userGuid = userInfoDictionary.objectForKey("guid") as String;
                self.avatarImageView.image = ImageUtils.retrieveUserImage(userGuid,reload: true);
                self.processComplete();
                }, errorHandle: { (error) -> Void in
                    self.processComplete();
                    Utils.debug("Error");
            });
        } else {
            self.nicknameTextField.text = userInfo.getNickname();
            self.gender = userInfo.getGender();
            self.gender = userInfo.getGender();
            if(self.gender == "FEMALE"){
                self.femaleButton.setImage(UIImage(named: "female_checked"), forState: UIControlState.Normal);
                self.maleButton.setImage(UIImage(named: "male_unchecked"), forState: UIControlState.Normal);
            } else {
                self.femaleButton.setImage(UIImage(named: "female_unchecked"), forState: UIControlState.Normal);
                self.maleButton.setImage(UIImage(named: "male_checked"), forState: UIControlState.Normal);
            }
            
            var avatarData = userInfo.getAvatar();
            if(avatarData != nil){
                var avatarImage = UIImage(data: avatarData!);
                self.avatarImageView.image = avatarImage;
                self.upload(avatarImage);
            } else {
                self.avatarImageView.image = UIImage(named: "avatar");
            }
        }
        
    }
    
    //Save user info
    @IBAction func saveUserInfo(sender: UIButton) {
        if(validate()){
            self.processing();
            let url = "/api/v1/profile"
            let nickname = self.nicknameTextField.text;
            var postDictionary = NSMutableDictionary();
            postDictionary.setObject(nickname, forKey: "nickname");
            postDictionary.setObject(self.gender, forKey: "gender");
            postDictionary.setObject("", forKey: "idolGuid");
            HttpUtils.sendRequest(url, method: RequestMethod.POST, httpBody: postDictionary, successHandle: { (response) -> Void in
                self.processComplete();
                var tabBarController = AuthorisationService.mainView();
                self.presentViewController(tabBarController, animated: true, completion: nil);
                }, errorHandle: { (error) -> Void in
                    self.processComplete();
            });
        }
    }
    
    func validate() -> Bool{
        var nickname = self.nicknameTextField.text;
        if(nickname.isEmpty){
            self.nicknameTextField.shake();
        }
        return !nickname.isEmpty;
    }
    
    // Choice avatar
    @IBAction func choiceAvatar(sender: UIButton){
        self.pickingImage();
    }
    
    override func didFinishPickingImage(image: UIImage!) {
        self.avatarImageView.image = image;
    }
    
    @IBAction func checkMale(sender: UIButton) {
        if(self.gender != "MALE"){
            self.gender = "MALE";
            sender.setImage(UIImage(named: "male_checked"), forState: UIControlState.Normal);
            self.femaleButton.setImage(UIImage(named: "female_unchecked"), forState: UIControlState.Normal);
        }
    }
    
    @IBAction func checkFemale(sender: UIButton) {
        if(self.gender != "FEMALE"){
            self.gender = "FEMALE";
            sender.setImage(UIImage(named: "female_checked"), forState: UIControlState.Normal);
            self.maleButton.setImage(UIImage(named: "male_unchecked"), forState: UIControlState.Normal);
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Utils.debug("Memory warning")
    }
}
