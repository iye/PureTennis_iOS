//
//  NormalView.swift
//  puretennis
//
//  Created by IYE Technologies on 1/16/15.
//  Copyright (c) 2015 IYE Technologies. All rights reserved.
//

import UIKit

class NormalView: UIView {
    
    var chosen = false;
    var shadeView:ShadeView?;
    var avatarImageView:UIImageView!;
    var nameLabel:UILabel!;
    var containerView:AppointmentScrollView!;
    
    var originalFrame:CGRect!;
    
    var element:NSDictionary!;
    
    var type:ContainerType?;
    
    var isOwner = false;
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame);
    }
    
    init(containerView:AppointmentScrollView,element:NSDictionary,type:ContainerType,ownerGuid:String) {
        super.init();
        
        var margin = NormalView.margin();
        
        var originX = containerView.contentSize.width;
        self.frame.origin = CGPoint(x: originX, y: margin);
        
        var height = containerView.frame.height - margin * 2;
        self.frame.size.height = height;
        
        self.type = type;
        self.element = element;
        self.layer
        self.containerView = containerView;
        
        
        var user:NSDictionary!;
        switch (type) {
        case .Invitees:
            user = element.objectForKey("invitee") as NSDictionary;
            break;
        case .Participants:
            user = element.objectForKey("user") as NSDictionary;
            
            break;
        default:
            user = element.objectForKey("sender") as NSDictionary;
            break;
        }
        
        var userGuid = user.objectForKey("guid") as String;
        self.isOwner = (userGuid == ownerGuid);
        
        var nickname = user.objectForKey("name") as String;
        self.nameLabel = UILabel()
        self.nameLabel.text = nickname;
        self.nameLabel.textColor = UIColor.whiteColor();
        self.nameLabel.font = Utils.systemFont(12);
        self.nameLabel.sizeToFit();
        self.nameLabel.textAlignment = NSTextAlignment.Center;
        self.nameLabel.frame.origin = CGPoint(x: 0, y: self.frame.height - self.nameLabel.frame.height);
        self.nameLabel.frame.size.width = self.frame.height - self.nameLabel.frame.height;
        self.addSubview(self.nameLabel);
        
        // Adjust self view width
        self.frame.size.width = self.frame.height - self.nameLabel.frame.height;
        
        var avatarFrame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.width);
        var avatarImageView = UIImageView(frame: avatarFrame);
        avatarImageView.layer.masksToBounds = true;
        avatarImageView.layer.cornerRadius = 4;
        avatarImageView.image = ImageUtils.retrieveUserImage(userGuid);
        self.avatarImageView = avatarImageView;
        self.addSubview(self.avatarImageView);
        
        
        self.originalFrame = self.frame;
        
        self.containerView.contentSize.width += (self.frame.width + margin);
    }
    
    func readyingChoice(){
        if(shadeView == nil && !isOwner){
            var frame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height);
            self.shadeView = ShadeView(frame: frame);
            self.addSubview(self.shadeView!);
        }
        self.avatarImageView.alpha = 0.8;
        self.shadeView?.show();
    }
    
    func cancelChoice(){
        self.avatarImageView.alpha = 1;
        self.shadeView?.removeFromSuperview();
        self.shadeView = nil;
        self.chosen = false;
    }
    
    func isMe() -> Bool{
        var invitee = element.objectForKey("invitee") as NSDictionary;
        var userGuid = invitee.objectForKey("guid") as String;
        var currentUserGuid = Utils.currentUserGuid();
        return (userGuid == currentUserGuid);
    }
    
    class func margin() -> CGFloat{
        return 5;
    }
}
