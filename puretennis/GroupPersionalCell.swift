//
//  GroupPersionalCell.swift
//  puretennis
//
//  Created by IYE Technologies on 1/13/15.
//  Copyright (c) 2015 IYE Technologies. All rights reserved.
//

import UIKit

class GroupPersionalCell: AbstractPersionalCell {

    class func create(width:CGFloat,user:NSMutableDictionary,ownerGuid:String) -> PersonalCell{
        var cell = PersonalCell();
        cell.setWidth(width);
        cell.setArrow("right_arrow_mini");
        
        var userGuid = user.objectForKey("guid") as String;
        var avatar = ImageUtils.retrieveUserImage(userGuid);
        cell.setAvatarImage(avatar);
        
        var name = user.objectForKey("name") as String;
        var isCurrentUser = Utils.isCurrentUser(userGuid);
        if(isCurrentUser){
            name = "我";
        }
        cell.setName(name);
        var isOwner = (ownerGuid == userGuid);
        if(isOwner){
            cell.setNameColor(UIColor.yellowColor());
        }
        return cell;
    }

}
