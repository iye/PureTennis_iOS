//
//  AppointmentNoteTextView.swift
//  puretennis
//
//  Created by IYE Technologies on 1/20/15.
//  Copyright (c) 2015 IYE Technologies. All rights reserved.
//

import UIKit

class AppointmentNoteTextView: UITextView {

    override func drawRect(rect: CGRect) {
        super.drawRect(rect);
        self.layer.masksToBounds = true;
        self.layer.cornerRadius = 4;
        self.layer.borderColor = UIColor.blueColor().CGColor;
        self.layer.borderWidth = 0.5;
        self.layer.shadowColor = UIColor.blueColor().CGColor;
        self.layer.shadowRadius = 4;
    }


}
