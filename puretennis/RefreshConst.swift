//
//  RefreshConst.swift
//  RefreshExample
//
//  Created by SunSet on 14-6-23.
//  Copyright (c) 2014 zhaokaiyuan. All rights reserved.
//

import UIKit

let RefreshViewHeight:CFloat = 64.0
let RefreshSlowAnimationDuration:NSTimeInterval = 0.3
let RefreshFooterPullToRefresh:NSString = "上拉加载更多"
let RefreshFooterReleaseToRefresh:NSString =  "松开立即加载更多"
let RefreshFooterRefreshing:NSString =  "正在加载..."
let RefreshHeaderPullToRefresh:NSString =  "上拉可以刷新"
let RefreshHeaderReleaseToRefresh:NSString =  "松开立即刷新"
let RefreshHeaderRefreshing:NSString =  "正在刷新中..."
let RefreshHeaderTimeKey:NSString =  "RefreshHeaderView"
let RefreshContentOffset:NSString =  "contentOffset"
let RefreshContentSize:NSString =  "contentSize"

let huawenhupo = UIFont(name: "STHupo", size: 14);

 