//
//  UserInfo.swift
//  puretennis
//
//  Created by IYE Technologies on 12/29/14.
//  Copyright (c) 2014 IYE Technologies. All rights reserved.
//

import UIKit

class UserInfo:NSObject {

    private var openId:String!;
    private var accessToken:String!;
    private var nickname:String!;
    private var gender:String!
    private var avatarURL:String!;
    
    
    init(openId:String,accessToken:String,nickname:String,gender:String,avatarURL:String) {
        super.init();
        self.openId = openId;
        self.accessToken = accessToken;
        self.nickname = nickname;
        self.gender = gender;
        self.avatarURL = avatarURL;
    }
    
    func getOpenId() -> String{
        return openId;
    }
    
    func getAccessToken() -> String{
        return accessToken;
    }
    
    func getNickname() -> String{
        return nickname;
    }
    
    func getGender() -> String{
        return gender;
    }
    
    func getAvatar() -> NSData?{
        var url = NSURL(string: avatarURL)!;
        return NSData(contentsOfURL: url);
    }

}
