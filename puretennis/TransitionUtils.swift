//
//  TransitionUtils.swift
//  puretennis
//
//  Created by Derek Zheng on 12/2/14.
//  Copyright (c) 2014 IYE Technologies. All rights reserved.
//

import Foundation
import UIKit

class TransitionUtils{
    class func slideInFrom(direction: String, view: UIView, completionDelegate: AnyObject? = nil) {
        // Create a CATransition animation
        let slideInFromTransition = CATransition()
        
        // Set its callback delegate to the completionDelegate that was provided (if any)
        if let delegate: AnyObject = completionDelegate {
            slideInFromTransition.delegate = delegate
        }
        
        // Customize slideInFromTransition animation's properties
        slideInFromTransition.type = kCATransitionPush
        
        if("L" == direction){
            slideInFromTransition.subtype = kCATransitionFromLeft
        }else{
            slideInFromTransition.subtype = kCATransitionFromRight
        }
        
        slideInFromTransition.duration = 0.3
        slideInFromTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        slideInFromTransition.fillMode = kCAFillModeRemoved
        
        // Add the animation to the View's layer
        if("L" == direction){
            view.layer.addAnimation(slideInFromTransition, forKey: "slideInFromLeftTransition")
        }else{
            view.layer.addAnimation(slideInFromTransition, forKey: "slideInFromRightTransition")
        }
    }
    
    class func slideInFromRight(view: UIView){
        slideInFrom("R", view: view, completionDelegate: nil);
    }
    
    class func slideInFromLeft(view: UIView){
        slideInFrom("L", view: view, completionDelegate: nil);
    }

}

