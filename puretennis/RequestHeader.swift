//
//  RequestHeader.swift
//  puretennis
//
//  Created by IYE Technologies on 11/13/14.
//  Copyright (c) 2014 IYE Technologies. All rights reserved.
//

import Foundation
class RequestHeader {
    private var field:String = "";
    
    private var value:String = "";
    
    init(field:String,value:String){
        self.field = field;
        self.value = value;
    }
    
    func setField(field:String){
        self.field = field;
    }
    
    func setValue(value:String){
        self.value = value;
    }
    
    func getField() -> String{
        return field;
    }
    
    func getValue() -> String{
        return value;
    }
    
    
}