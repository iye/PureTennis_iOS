//
//  GlobalVariables.swift
//  puretennis
//
//  Created by Derek Zheng on 1/7/15.
//  Copyright (c) 2015 IYE Technologies. All rights reserved.
//

import Foundation
import UIKit

struct GlobalVariables {
    static var currentUserDetails: UserDetailsViewController?
    static var timer: NSDate?    
    static var friendViewController: FriendsViewController?
    static var groupViewController: GroupViewController?
}
