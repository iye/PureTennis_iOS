//
//  ViewUtils.swift
//  puretennis
//
//  Created by Derek Zheng on 12/4/14.
//  Copyright (c) 2014 IYE Technologies. All rights reserved.
//

import Foundation
import UIKit

class ViewUtils{
            
    class func addBackgroundView(viewController: UIViewController) -> Void{
        var backgroundImageView = UIImageView(image: UIImage(named: "background"))
        backgroundImageView.frame = UIScreen.mainScreen().bounds;
        viewController.view.insertSubview(backgroundImageView, atIndex: 0)
    }
    
    class func statusBarHeight() -> CGFloat{
        return UIApplication.sharedApplication().statusBarFrame.height;
    }
    
    class func titleView(title:String) -> UILabel{
        return self.label(title, fontSize: 18)
    }
    
    class func label(text:String, fontSize: CGFloat) -> UILabel{
        let titleLabel = UILabel();
        titleLabel.font = Utils.fontHupo(fontSize);
        titleLabel.textColor = UIColor.whiteColor();
        titleLabel.textAlignment = NSTextAlignment.Center;
        titleLabel.text = text;
        titleLabel.sizeToFit();
        return titleLabel;
    }
    

    class func getTopController() -> UIViewController {
        var topViewController = UIApplication.sharedApplication().keyWindow?.rootViewController
        while((topViewController?.presentedViewController) != nil){
            topViewController = topViewController?.presentedViewController
        }
        
        return topViewController!
    }
    
    class func reloadBages() {
        var topController = getTopController()
        if(topController.isKindOfClass(PureTennisTabBarController)){
            (topController as PureTennisTabBarController).loadBadges()
        }
    }
    
    

    class func colorWithHexString (hex:String) -> UIColor {
        var cString:String = hex.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()).uppercaseString
        
        if (cString.hasPrefix("#")) {
            cString = (cString as NSString).substringFromIndex(1)
        }
        
        if (countElements(cString) != 6) {
            return UIColor.grayColor()
        }
        
        var rString = (cString as NSString).substringToIndex(2)
        var gString = ((cString as NSString).substringFromIndex(2) as NSString).substringToIndex(2)
        var bString = ((cString as NSString).substringFromIndex(4) as NSString).substringToIndex(2)
        
        var r:CUnsignedInt = 0, g:CUnsignedInt = 0, b:CUnsignedInt = 0;
        NSScanner(string: rString).scanHexInt(&r)
        NSScanner(string: gString).scanHexInt(&g)
        NSScanner(string: bString).scanHexInt(&b)
        
        
        return UIColor(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: CGFloat(1))
    }
}