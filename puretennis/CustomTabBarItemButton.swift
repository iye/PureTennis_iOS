//
//  CustomTabBarItemButton.swift
//  puretennis
//
//  Created by IYE Technologies on 11/26/14.
//  Copyright (c) 2014 IYE Technologies. All rights reserved.
//

import UIKit

class CustomTabBarItemButton: UIButton {
    
    // Badge view
    var badge:CustomBadgeView?;
    
    var badgeAmount: Int?
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
    }
    
    override init(frame:CGRect){
        super.init(frame:frame);
        self.imageView?.contentMode = UIViewContentMode.Center;
    }
    
    func setBadgeValue(badge: Int){
        self.badgeAmount = badge
        
        if(self.badge == nil){
            self.badge = CustomBadgeView.create(self.frame);
            self.addSubview(self.badge!);
        }
        
        if(badge > 0){
            self.badge!.setBadge(badge);
            self.badge?.hidden = false
        }else{
            self.badge?.hidden = true
        }
    }
    
    func updateBadge(){
        if(self.badgeAmount > 0){
            var amount = self.badgeAmount! - 1;
            self.setBadgeValue(amount);
        }
    }
}
