//
//  DayPeriod.swift
//  puretennis
//
//  Created by Derek Zheng on 12/2/14.
//  Copyright (c) 2014 IYE Technologies. All rights reserved.
//

import Foundation

enum DayPeriod : String{
    
    case MORNING = "MORNING"
    case AFTERNOON = "AFTERNOON"
    case EVENING = "EVENING"
    
}