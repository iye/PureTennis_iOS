//
//  SearchResultsTableDelegateAndDataSource.swift
//  puretennis
//
//  Created by IYE Technologies on 11/27/14.
//  Copyright (c) 2014 IYE Technologies. All rights reserved.
//

import UIKit

class SearchResultsTableDelegateAndDataSource:NSObject,UITableViewDataSource,UITableViewDelegate {
    
    var results:NSMutableArray = [];
    
    var navigationController:UINavigationController?;
    
    var searchBox:SearchBox!;
    
    init(searchBox:SearchBox,navigationController:UINavigationController){
        super.init();
        self.searchBox = searchBox;
        self.navigationController = navigationController;
    }
   
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return results.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        var group = results[indexPath.row] as NSMutableDictionary;
        var groupName = group.objectForKey("groupName") as String;
        var participantsAmount = group.objectForKey("participantsAmount") as Int;
        var cell = PureTextCell.create(tableView.frame.width, leftText: groupName, rightText: "\(participantsAmount)人");
        return cell;
    }
    
    func setResults(results:NSMutableArray){
        self.results = results;
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        self.searchBox.textField.resignFirstResponder();
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var storyboard = UIStoryboard(name: "GroupDetails", bundle: nil);
        var groupDetailsAsParticipantView = storyboard.instantiateViewControllerWithIdentifier("groupDetailsAsParticipantView") as GroupDetailsAsParticipantViewController;
        var group = results[indexPath.row] as NSMutableDictionary;
        var groupInfo = NSMutableDictionary();
        groupInfo.setObject(group.objectForKey("groupGuid")!, forKey: "guid");
        groupInfo.setObject(group.objectForKey("groupName")!, forKey: "name");
        groupDetailsAsParticipantView.setup(groupInfo);
        self.navigationController?.pushViewController(groupDetailsAsParticipantView, animated: true);
    }
}
