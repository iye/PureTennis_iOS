//
//  IYEDatePicker.swift
//  CustomDatePickerDemo
//
//  Created by IYE Technologies on 12/19/14.
//  Copyright (c) 2014 IYE Technologies. All rights reserved.
//

import UIKit

class AppointmentCustomDatePicker: AbstractPickerView {
    
    var datePicker = AppointmentDatePicker();
    var firstPeriodPicker = AppointmentTimePicker();
    var secondPeriodPicker = AppointmentTimePicker();
    
    var date = NSDate();
    var firstPeriod = NSDate();
    var secondPeriod = NSDate();
    
    var firstPeriodAsString:String!;
    var secondPeriodAsString:String!;
    
    var value:String!;
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder);
    }
    
    override init(frame:CGRect){
        super.init(frame: frame);
        margin = 20;
        var datePickerWidth:CGFloat = frame.width - margin * 2;
        self.datePicker.frame = CGRect(x: margin, y: 0, width: datePickerWidth, height: margin);
        self.contentView.addSubview(self.datePicker);
        
        var labelY = self.datePicker.frame.origin.y + self.datePicker.frame.height;
        var startLabel = UILabel();
        startLabel.frame.origin = CGPoint(x: margin, y: labelY);
        startLabel.text = "开始时间";
        startLabel.sizeToFit();
        self.contentView.addSubview(startLabel);
        
        var endLabel = UILabel();
        endLabel.text = "结束时间";
        endLabel.sizeToFit();
        endLabel.frame.origin = CGPoint(x: frame.width - margin - endLabel.frame.width, y: labelY);
        self.contentView.addSubview(endLabel);
        
        
        margin = 40;
        var originY = self.datePicker.frame.origin.y + self.datePicker.frame.height;
        var width = (frame.width - margin * 3) / 2;
      
        self.firstPeriodPicker.frame = CGRect(x: margin, y: originY, width: width, height: margin);
        self.contentView.addSubview(self.firstPeriodPicker);
    
        self.secondPeriodPicker.frame = CGRect(x: self.firstPeriodPicker.frame.origin.x + self.firstPeriodPicker.frame.width + margin , y: originY, width: width, height: margin);
        self.contentView.addSubview(self.secondPeriodPicker);
        
        self.datePicker.defaultSelect();
        self.firstPeriodPicker.defaultSelect();
        
        var hour = self.firstPeriodPicker.hour;
        self.secondPeriodPicker.defaultSelect();
        self.secondPeriodPicker.hour = hour + 2;
        
        
        self.firstPeriodAsString = self.firstPeriodPicker.value;
        self.secondPeriodAsString = self.secondPeriodPicker.value;
    }
    
    override func confirm(sender:UIButton){
        self.date = self.datePicker.date;
        
        var month = self.datePicker.month;
        var monthAsString = month < 10 ? "0\(month)" : "\(month)";
        var day = self.datePicker.day;
        var dayAsString = day < 10 ? "0\(day)" : "\(day)";
        var dateAsLabelValue = "\(self.datePicker.year)年\(monthAsString)月\(dayAsString)日";
        
        self.firstPeriodAsString = self.firstPeriodPicker.value;
        self.secondPeriodAsString = self.secondPeriodPicker.value;
        self.value = "\(dateAsLabelValue) \(firstPeriodAsString) - \(secondPeriodAsString)";
        sendActionsForControlEvents(UIControlEvents.ValueChanged);
        self.hide();
    }
    
    override func show() {
        
        self.hidden = false;
        UIView.animateWithDuration(0.3, animations: { () -> Void in
            self.frame = self.showFrame;
        });
        UIView.commitAnimations();
    }
}
