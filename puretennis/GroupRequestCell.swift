//
//  GroupRequestCell.swift
//  puretennis
//
//  Created by IYE Technologies on 1/5/15.
//  Copyright (c) 2015 IYE Technologies. All rights reserved.
//

import UIKit

class GroupRequestCell: AbstractPersionalCell {
   
    class func create(idnexPath:NSIndexPath,width:CGFloat,request:NSDictionary) -> GroupRequestCell{
        var cell = GroupRequestCell();
        cell.setWidth(width);
        cell.setArrow("question");
        
        var sender = request.objectForKey("sender") as NSDictionary;
        var senderGuid = sender.objectForKey("guid") as String;
        var avatar = ImageUtils.retrieveUserImage(senderGuid);
        cell.setAvatarImage(avatar);
        
        var senderName = sender.objectForKey("name") as String;
        cell.setName(senderName);
        return cell;
    }
}
