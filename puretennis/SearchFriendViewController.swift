//
//  SearchFriendViewController.swift
//  puretennis
//
//  Created by Derek Zheng on 12/9/14.
//  Copyright (c) 2014 IYE Technologies. All rights reserved.
//

import UIKit

class SearchFriendViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var backgroundView = UIView();
    
    var searchInputBox: SearchBox?
    
    var friendList: [NSMutableDictionary] = []
    
    var friendListView: UITableView!;
    
    var friendDetailsViewController = UserDetailsViewController()
    
    override func viewDidLoad() {
        self.initNavigation();
    }
    
    func initNavigation(){
        var backButton = UIButton();
        backButton.setImage(UIImage(named: "back"), forState: UIControlState.Normal);
        backButton.showsTouchWhenHighlighted = true;
        backButton.sizeToFit();
        backButton.addTarget(self, action: Selector("back:"), forControlEvents: UIControlEvents.TouchUpInside);
        var backBarButton = UIBarButtonItem(customView: backButton);
        self.navigationItem.leftBarButtonItem = backBarButton;
    }
    
    func back(sender:UIButton){
        self.navigationController?.popViewControllerAnimated(true);
    }
    
    override func viewDidAppear(animated: Bool) {
        self.navigationItem.titleView = ViewUtils.titleView("添加好友")
    }
    
    override func viewWillAppear(animated: Bool) {
        ViewUtils.addBackgroundView(self)
        self.backgroundView.frame.origin.x = 0
        self.backgroundView.frame.origin.y = (self.navigationController?.navigationBar.frame.height)! + ViewUtils.statusBarHeight();
        
        self.view.addSubview(backgroundView);
        
        if(self.searchInputBox == nil){
            self.searchInputBox = SearchBox(searchCallback: searchFriends, placeholder: "查找小伙伴");
            self.view.addSubview(searchInputBox!);
        }
        if(self.friendListView == nil){
            self.friendListView = UITableView();
            friendListView.separatorStyle = UITableViewCellSeparatorStyle.None
            friendListView.backgroundColor = UIColor.clearColor()
            friendListView.delegate = self
            friendListView.dataSource = self
            
            var margin:CGFloat = 5;
            var navigationBarHeight = (self.navigationController?.navigationBar.frame.height)! + ViewUtils.statusBarHeight();
            friendListView.frame.origin = CGPoint(x: margin, y: searchInputBox!.frame.origin.y + searchInputBox!.frame.height + 10);
            friendListView.frame.size = CGSize(width: self.view.frame.size.width - margin * 2, height: self.view.frame.size.height * 0.7);
            self.view.addSubview(friendListView);
        }
        
        
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return friendList.count;
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        var friend = friendList[indexPath.row] as NSMutableDictionary;
        
        var guid = friend.objectForKey("guid") as String
        friendDetailsViewController.guid = guid
        
        friendDetailsViewController.loadUserDetails()
        self.navigationController?.pushViewController(self.friendDetailsViewController, animated: true)
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        var friend = friendList[indexPath.row] as NSMutableDictionary;
        var guid = friend.objectForKey("guid") as String
        var nickname = friend.objectForKey("nickname") as String;
        
        var user = NSMutableDictionary();
        user.setObject(guid, forKey: "guid");
        user.setObject(nickname, forKey: "name");
        var cell = PersonalCell.create(tableView.frame.width, user: user);
        return cell;
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool{
        return false;
    }
  
    
    func sendAddFriendRequest(tableViewRowAction:UITableViewRowAction!,indexPath:NSIndexPath!){
        Utils.debug(indexPath.row);
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath){
        
    }
    
    func searchFriends(keyword: String){
        self.processing();
        var url = "/api/v1/friends/search"
        
        var body = NSMutableDictionary();
        body.setObject(keyword, forKey: "keyword");

        HttpUtils.sendRequest(url, method: RequestMethod.POST, httpBody: body, successHandle: { (response) -> Void in
            
            self.friendList = response as [NSMutableDictionary]
            if(self.friendList.count == 0){
                self.searchInputBox?.notFound("没找到相关的小伙伴");
            }
            self.friendListView.reloadData()
            self.processComplete();
            }, errorHandle: { (error) -> Void in
                self.processComplete();
        });
    }
}
