//
//  SearchInputBox.swift
//  puretennis
//
//  Created by IYE Technologies on 12/3/14.
//  Copyright (c) 2014 IYE Technologies. All rights reserved.
//

import UIKit

class SearchBox: UIView,UITextFieldDelegate {
    var backgroupd:UIImageView!;
    
    var searchImageView:UIImageView!;
    var textField:UITextField!;
    var clearButton:UIButton!;
    
    var placeholder:String?;
    
    var notFoundView:UIView?;
    
    var searchCallback:((keywords:String) -> Void)?;
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
    }
    
    init(searchCallback:((keywords:String) -> Void)?,placeholder:String?){
        self.placeholder = placeholder;
        self.searchCallback = searchCallback;
        
        var margin:CGFloat = 5;
        let width = UIScreen.mainScreen().bounds.width - margin * 2;
        let searchBoxY = (44 + ViewUtils.statusBarHeight()) * 1.1;
        let searchBoxFrame = CGRect(x: margin, y: searchBoxY, width: width, height: 35);
        super.init(frame: searchBoxFrame);
       
        
        self.backgroupd = UIImageView(image: UIImage(named: "search_frame"));
        self.backgroupd.frame.size = CGSize(width: frame.width, height: frame.height);
        self.addSubview(backgroupd);
        
        let backgroupdHeight = backgroupd.frame.height;
        let backgroundWidth = backgroupd.frame.width;
        
        self.frame.size.height = backgroupdHeight;
        
        self.searchImageView = UIImageView(image: UIImage(named: "search"));
        self.searchImageView.sizeToFit();
        
        let searchImageViewY = (self.backgroupd.frame.height - self.searchImageView.frame.height) / 2;
        self.searchImageView.frame.origin = CGPoint(x: 5, y: searchImageViewY);
        
        self.addSubview(searchImageView);
        
        self.clearButton = UIButton();
        self.clearButton.frame.origin = CGPoint(x: self.backgroupd.frame.width - backgroupdHeight, y: 0);
        self.clearButton.frame.size = CGSize(width: backgroupdHeight, height: backgroupdHeight);
        self.clearButton.setImage(UIImage(named: "clear"), forState: UIControlState.Normal);
        self.clearButton.addTarget(self, action: Selector("clear:"), forControlEvents: UIControlEvents.TouchUpInside);
        self.clearButton.hidden = true;
        
        let textFieldX = self.searchImageView.frame.origin.x + self.searchImageView.frame.width + 2;
        let textFieldWidth = self.frame.width - textFieldX - (self.clearButton.frame.width - 8);
        self.textField = UITextField(frame: CGRect(x: textFieldX, y: 0, width: textFieldWidth, height: backgroupdHeight));
        self.textField.font = Utils.defaultFont();
        self.textField.textColor = UIColor.whiteColor();
        self.textField.returnKeyType = UIReturnKeyType.Search;
        self.textField.delegate = self;
        if(placeholder != nil){
            setPlaceholder();
        }
        
        self.addSubview(searchImageView);
        self.addSubview(textField);
        self.addSubview(clearButton);
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("textFieldDidChange:"), name: UITextFieldTextDidChangeNotification, object: nil);
    }
    
    func textFieldDidChange(notification:NSNotification){
        self.clearButton.hidden = self.textField.text.isEmpty
    }
    
    func clear(sender:UIButton){
        self.textField.text = "";
        sender.hidden = true;
    }
    
    private func setPlaceholder(){
        var attributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        self.textField.attributedPlaceholder = NSAttributedString(string:placeholder!, attributes:attributes);
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        textField.placeholder = "";
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if(!textField.text.isEmpty){
            self.notFoundView?.removeFromSuperview();
            self.searchCallback!(keywords: textField.text);
            textField.resignFirstResponder();
            self.clearButton.hidden = false;
        }
        return true;
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        if(textField.text.isEmpty){
            setPlaceholder();
            self.clearButton.hidden = true;
        }
    }
    
    func notFound(title:String){
        var frame = CGRect(x: self.frame.origin.x, y: self.frame.origin.y + self.frame.height + 5, width: self.frame.width, height: self.frame.height);
        var label = UILabel(frame: frame);
        label.text = title;
        label.textAlignment = NSTextAlignment.Center;
        label.textColor = UIColor.whiteColor();
        label.font = Utils.defaultFont();
        self.notFoundView = label;
        self.superview?.addSubview(self.notFoundView!);
        
    }
    
}
