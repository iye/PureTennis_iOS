//
//  RequestTableViewDataSource.swift
//  puretennis
//
//  Created by IYE Technologies on 11/25/14.
//  Copyright (c) 2014 IYE Technologies. All rights reserved.
//

import UIKit

class RequestsDataSourceAndDelegate: NSObject, UITableViewDataSource, UITableViewDelegate {
    
    var requests:NSMutableArray = NSMutableArray();
    
    override init() {
        super.init();
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return requests.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCellWithIdentifier("requestCell") as UITableViewCell;
        
        let request = requests[indexPath.row] as NSMutableDictionary;
        let friendName = request.objectForKey("name") as String;
        cell.textLabel!.text = friendName;
        
        return cell;
    }
    
    
    func addRequests(requests:[NSMutableDictionary]){
        self.requests.addObjectsFromArray(requests);
    }
    
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true;
    }
    
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [AnyObject]? {
        let rejectRequestAction = UITableViewRowAction(style: UITableViewRowActionStyle.Normal, title: "拒绝") { (tableViewRowAction, indexPath) -> Void in
            self.rejectRequestHandler(tableViewRowAction,indexPath: indexPath,tableView: tableView);
        }
        
        rejectRequestAction.backgroundColor = UIColor.redColor();
        
        let acceptRequestAction = UITableViewRowAction(style: UITableViewRowActionStyle.Normal, title: "同意") { (tableViewRowAction, indexPath) -> Void in
            self.acceptRequestHandler(tableViewRowAction,indexPath: indexPath,tableView: tableView);
            
        }
        acceptRequestAction.backgroundColor = UIColor.blueColor();
        return [acceptRequestAction,rejectRequestAction];
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    
    // Reject request handler is here
    func rejectRequestHandler(tableViewRowAction:UITableViewRowAction!,indexPath:NSIndexPath!,tableView:UITableView){
        let url = "/api/v1/friends/request/reject";
        
        requestOperation(url,operation: RequestOpertion.Reject,tableView: tableView,indexPath: indexPath);
        
        tableView.editing = false;

    }
    
    // Accept request handler is here
    func acceptRequestHandler(tableViewRowAction:UITableViewRowAction!,indexPath:NSIndexPath!,tableView:UITableView){
        let url = "/api/v1/friends/request/accept";
        
        requestOperation(url,operation: RequestOpertion.Reject,tableView: tableView,indexPath: indexPath);

    }
    
    func requestOperation(url:String, operation:RequestOpertion,tableView:UITableView,indexPath:NSIndexPath){
        let request = requests[indexPath.row] as NSMutableDictionary;
        let guid = request.objectForKey("guid") as String;
        
        let postData = NSMutableDictionary();
        postData.setObject(guid, forKey: "guid");
       
        Utils.sendRequest(url, requestMethod: "POST", httpBody: postData, successHandle: { (response) -> Void in
            self.requests.removeObjectAtIndex(indexPath.row);
            tableView.beginUpdates();
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Top);
            tableView.endUpdates();
            tableView.editing = false;
        }, errorHandle: { (error) -> Void in
            
        });
    }
    
    enum RequestOpertion{
        case Reject;
        case Accept;
    }
}
