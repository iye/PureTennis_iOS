//
//  LoginService.swift
//  puretennis
//
//  Created by IYE Technologies on 11/12/14.
//  Copyright (c) 2014 IYE Technologies. All rights reserved.
//

import Foundation
import UIKit
import AudioToolbox
import CoreData

class Utils:NSObject {
   
    class func saveData(value:AnyObject,key:String){
        CacheData.saveOrUpdate(key, value: value)
    }
    
    class func getValue(key:String) -> AnyObject?{
        return CacheData.query(key)
    }
    
    class func removeData(key:String){
        return CacheData.remove(key)
    }
    
    class func encodeUsernamePassword(username:String,password:String)->String{
        let value = username+":"+password;
        
        let utf8str = value.dataUsingEncoding(NSUTF8StringEncoding);
        
        let base64Encoded = utf8str?.base64EncodedDataWithOptions(NSDataBase64EncodingOptions(rawValue: 0));
        
        let encodedAsString = "Basic " + NSString(data: base64Encoded!, encoding: NSUTF8StringEncoding)!;
        
        return encodedAsString;
    }
    
    class func base64String(image:UIImage) ->String {
        let imageData = UIImagePNGRepresentation(image);
        return imageData.base64EncodedStringWithOptions( NSDataBase64EncodingOptions.allZeros);
    }
    
    class func fromBase64String(base64String:String) ->NSData{
        return NSData(base64EncodedString: base64String, options: NSDataBase64DecodingOptions.IgnoreUnknownCharacters)!;
    }
    
    
    // STHupo(Hua wen hu po)
    class func fontHupo(size:CGFloat) -> UIFont {
        return UIFont(name: "STHupo", size: size)!;
    }
    
    class func systemFont(size: CGFloat) -> UIFont{
        return UIFont(name: "HelveticaNeue", size: size)!
    }
    
    class func defaultFont() -> UIFont {
        return UIFont(name: "STHupo", size: Properties.DEFAULT_FONT_SIZE)!;
    }
    
    class func imageFromBase64String(base64:String) -> UIImage{
        let data = NSData(base64EncodedString: base64, options: NSDataBase64DecodingOptions.IgnoreUnknownCharacters)!;
       return UIImage(data: data)!;
    }
    
    class func isCurrentUser(userGuid:String) -> Bool{
        var localUserGuid = getValue("guid") as String;
        return localUserGuid == userGuid;
    }
    
    class func isOwner(ownerGuid:String) -> Bool{
        return isCurrentUser(ownerGuid);
    }
    
    class func userNameResolve(user:NSMutableDictionary) -> String{
        var creatorGuid = user.objectForKey("guid") as String;
        var currentUserGuid = Utils.getValue("guid") as String;
        var isCurrentUser = Utils.isCurrentUser(creatorGuid);
        if(isCurrentUser){
            return "我";
        }
        return user.objectForKey("name") as String;
    }
   
    
    class func systemVersion() -> Double{        
        return  (UIDevice.currentDevice().systemVersion as NSString).doubleValue
    }
    
    class func debug<T> (object: T, filename: String? = __FILE__, line: Int? = __LINE__){
//        let formatter  = NSDateFormatter();
//        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss";
//        let date = formatter.stringFromDate(NSDate());
//        println("\(date) [\(filename!.lastPathComponent):\(line!)]: \(object)")
    }
    
    class func playVibrate() {
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
    }
    
    class func jsonToNSDictionary(text: String) -> NSDictionary? {
        var json = text.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
        return NSJSONSerialization.JSONObjectWithData(json!, options: NSJSONReadingOptions.MutableContainers, error: nil) as? NSDictionary
    }
    
    class func currentUserGuid() -> String{
        return getValue("guid") as String;
    }
}
