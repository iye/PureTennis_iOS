//
//  CreatGroupViewController.swift
//  puretennis
//
//  Created by IYE Technologies on 11/25/14.
//  Copyright (c) 2014 IYE Technologies. All rights reserved.
//

import UIKit

class CreateOrUpdateGroupViewController: UIViewController {

    @IBOutlet weak var groupNameTextField: UITextField!
    
    @IBOutlet weak var accessPublicSwitch: UISwitch!
    
    @IBOutlet weak var accessFriendOnlySwitch: UISwitch!
    
    @IBOutlet weak var groupDescriptionTextField: UITextField!
    
    var groupGuid = "";
    var access:String = "PUBLIC";
    var name = "";
    var groupDescription = "";
    
    var adjustContentToAccommodateKeyboard:AdjustContentToAccommodateKeyboard!;
    override func viewDidLoad() {
        super.viewDidLoad();
        ViewUtils.addBackgroundView(self);
        self.initNavigation();
        self.adjustContentToAccommodateKeyboard = AdjustContentToAccommodateKeyboard(target: self);
        self.fillGroupInfo();
    }
    
    func fillGroupInfo(){
        if(self.access == "PUBLIC"){
            self.accessPublicSwitch.setOn(true, animated: false);
            self.accessFriendOnlySwitch.setOn(false, animated: false);
        }
        
        if(self.access == "FRIEND_ONLY"){
            self.accessPublicSwitch.setOn(false, animated: false);
            self.accessFriendOnlySwitch.setOn(true, animated: false);
        }
        self.groupNameTextField.text = name;
        self.groupDescriptionTextField.text = groupDescription;
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated);
        self.hideTabBar();
    }
    
    func create(){
        self.setTitle("创建群");
    }
    
    // Edit group
    func edit(group:NSDictionary){
        self.groupGuid = group.objectForKey("guid") as String;
        self.name = group.objectForKey("name") as String;
        self.access = group.objectForKey("access") as String;
        self.groupDescription = group.objectForKey("description") as String;
        self.setTitle(name);
    }
    
    func setTitle(title:String){
        self.navigationItem.titleView = ViewUtils.titleView(title);
    }
    
    func initNavigation(){
        
        let backButton = UIButton();
        backButton.setImage(UIImage(named: "back"), forState: UIControlState.Normal);
        backButton.addTarget(self, action: Selector("backBarButtonHandler:"), forControlEvents: UIControlEvents.TouchUpInside);
        backButton.showsTouchWhenHighlighted = true;
        backButton.sizeToFit();
        let backBarButtonItem = UIBarButtonItem(customView: backButton);
        
        let saveGroupButton = UIButton();
        saveGroupButton.setImage(UIImage(named: "confirm"), forState: UIControlState.Normal);
        saveGroupButton.showsTouchWhenHighlighted = true;
        saveGroupButton.sizeToFit();
        saveGroupButton.addTarget(self, action: Selector("saveGroupButtonHandler:"), forControlEvents: UIControlEvents.TouchUpInside);
        
        let saveBarButtonItem = UIBarButtonItem(customView: saveGroupButton);
        self.navigationItem.leftBarButtonItem = backBarButtonItem;
        self.navigationItem.rightBarButtonItem = saveBarButtonItem;
        

    }

    // Back operation handler
    func backBarButtonHandler(barButtonItem:UIBarButtonItem){
        self.navigationController!.popViewControllerAnimated(true);
    }
    
    // Save group operation handler
    func saveGroupButtonHandler(barButtonItem:UIBarButtonItem){
        self.saveGroup();
    }

    
    // Save group
    func saveGroup(){
        self.processing();
        var url = "/api/v1/group";
        var groupPostData = NSMutableDictionary();
        var groupName = groupNameTextField.text;
        var groupDescription = groupDescriptionTextField.text;
        groupPostData.setObject(self.groupGuid, forKey: "groupGuid");
        groupPostData.setObject(groupName, forKey: "name");
        groupPostData.setObject(access, forKey: "access");
        groupPostData.setObject(groupDescription, forKey: "description");
        HttpUtils.sendRequest(url, method: RequestMethod.POST, httpBody: groupPostData, successHandle: saveGroupSuccess, errorHandle: { (error) -> Void in
            self.processComplete();
        });
    }
    
    func saveGroupSuccess(response:AnyObject?){
        self.processComplete();
        if(self.groupGuid.isEmpty){
            self.navigationController?.popToRootViewControllerAnimated(true);
        } else {
            self.navigationController?.popViewControllerAnimated(true);
        }
    }
    
    
    // AccessPublicSwitchChanged
    @IBAction func accessPublicSwitchChanged(sender: UISwitch) {
        let on = sender.on;
        accessFriendOnlySwitch.setOn(!on, animated: true);
        if(on){
            self.access = "PUBLIC";
        } else {
            self.access = "FRIEND_ONLY";
        }
    }
    
    //AccessFriendOnlySwithcChanged
    @IBAction func accessFriendOnlySwitchChanged(sender: UISwitch) {
        let on = sender.on;
        accessPublicSwitch.setOn(!on, animated: true);
        if(on){
            self.access = "FRIEND_ONLY";
        } else {
            self.access = "PUBLIC";
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Utils.debug("Memory warning")
        // Dispose of any resources that can be recreated.
    }
}
