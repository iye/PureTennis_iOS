//
//  CustomTextField.swift
//  puretennis
//
//  Created by IYE Technologies on 12/5/14.
//  Copyright (c) 2014 IYE Technologies. All rights reserved.
//

import UIKit

class CustomTextField: UITextField,UITextFieldDelegate {
    
    var originalPlaceholder:String!;
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
        self.initTextField();
    }
    
    override init() {
        super.init();
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame);
    }
    
    func initTextField(){
        self.addPlaceholder(self.placeholder);
    }
   
    override func textRectForBounds(bounds: CGRect) -> CGRect {
        return CGRectInset(bounds, 10, 10);
    }
    
    override func editingRectForBounds(bounds: CGRect) -> CGRect {
        return CGRectInset(bounds, 10, 10);
    }
    
    func addPlaceholder(placeholder:String?){
        if(placeholder != nil){
            self.originalPlaceholder = placeholder;
            var attributes = [NSForegroundColorAttributeName: UIColor.whiteColor()];
            self.attributedPlaceholder = NSAttributedString(string:placeholder!, attributes:attributes);
        }
    }
    
    func hidePlaceholder(){
        self.placeholder = "";
    }
    
    func showPlaceholder(){
        var attributes = [NSForegroundColorAttributeName: UIColor.whiteColor()];
        self.attributedPlaceholder = NSAttributedString(string:self.originalPlaceholder, attributes:attributes);
    }
}
