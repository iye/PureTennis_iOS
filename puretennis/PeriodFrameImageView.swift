//
//  PeriodFrameImageView.swift
//  puretennis
//
//  Created by Derek Zheng on 12/2/14.
//  Copyright (c) 2014 IYE Technologies. All rights reserved.
//

import UIKit

class PeriodFrameImageView: UIImageView {
        
    var status: DayPeriodStatus?
    
    var label: DayPeriod?
    
    var day: DayAvailabilityImageView?
    
    var availabilityImageView: UIImageView?
    
    
    init(day: DayAvailabilityImageView, status: DayPeriodStatus, label: DayPeriod){
        
        super.init(image: UIImage(named: "frame"))
        self.day = day;
        self.status = status;
        self.label = label;
        
        var index = 1 as CGFloat;
        
        if(label == DayPeriod.AFTERNOON){
            index = 2;
        }else if(label == DayPeriod.EVENING){
            index = 3;
        }
        
        
        self.frame.size = day.frame.size
        self.frame.origin = CGPoint(x: day.frame.origin.x + self.frame.width*index, y: day.frame.origin.y)
        self.userInteractionEnabled = true;
        
        var tapGesture = UITapGestureRecognizer(target: self, action: Selector("onTouched:"));
        tapGesture.numberOfTapsRequired = 1;
        tapGesture.numberOfTouchesRequired = 1;
        
        self.addGestureRecognizer(tapGesture);
        
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
    func onTouched(image: PeriodFrameImageView){
        var date = day?.date;
        var period = self.label?.rawValue;
        var today = DateUtils.now();
        var dateObj = day?.dateObj;
        var interval = dateObj?.timeIntervalSinceDate(today);

        
        var body = NSMutableDictionary();
        body.setObject(date!, forKey: "date");
        body.setObject(period!, forKey: "period");

        var url = "/api/v1/user/availability";

        HttpUtils.sendRequest(url, method: RequestMethod.POST, httpBody: body, successHandle: { (response) -> Void in

            var result = response as NSMutableDictionary;
            var statusText = result.objectForKey("availability") as String
            self.onStatusChange(DayPeriodStatus.toStatus(statusText))
            
        }, errorHandle: { (error) -> Void in
            Utils.debug("Error happens!");
        })
        
    }
    
    func onStatusChange(status: DayPeriodStatus){
        
        self.status = status
        
        switch status{
            
            case .YES:
                availabilityImageView = UIImageView(image:UIImage(named: "ball"))
                self.addSubview(availabilityImageView!)
            
            case .SCHEDULED:
                availabilityImageView = UIImageView(image:UIImage(named: "confirm"))
                self.addSubview(availabilityImageView!)
            
            default:
                availabilityImageView?.removeFromSuperview();
                availabilityImageView = nil
        }
        
        
        let size = self.frame.height * 0.6
        availabilityImageView?.frame.size = self.frame.size;
        availabilityImageView?.frame.size.width = size
        availabilityImageView?.frame.size.height = size
        availabilityImageView?.frame.origin = CGPoint(x: (self.frame.width - size)/2 , y: (self.frame.height - size)/2)


        
    }
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    
    

}
