//
//  AppointmentCell.swift
//  puretennis
//
//  Created by IYE Technologies on 12/31/14.
//  Copyright (c) 2014 IYE Technologies. All rights reserved.
//

import UIKit

class AppointmentCell: AbstractPersionalCell {
    
    var detailsLabel:UILabel!;
    
    var appointment:NSDictionary!;
    
    var participantsAmount:Int!;
    
    var indexPath:NSIndexPath!;
    
    var acceptInvitationCallback:((indexPath:NSIndexPath)->Void)!;
    
    class func create(indexPath:NSIndexPath,appointment:NSDictionary,width:CGFloat) -> AppointmentCell{
        
        var cell = AppointmentCell();
        cell.appointment = appointment;
        cell.indexPath = indexPath;
        cell.setWidth(width);
        cell.setArrow("right_arrow_mini");
        var creator = appointment.objectForKey("creator") as NSMutableDictionary;
        
        var creatorGuid = creator.objectForKey("guid") as String;
        var avatar = ImageUtils.retrieveUserImage(creatorGuid);
        cell.setAvatarImage(avatar);
        
        var creatorName = Utils.userNameResolve(creator);
        var location = appointment.objectForKey("location") as String;
        cell.setName("\(creatorName)\t\(location)");
        var currentParticipantsAmount = appointment.objectForKey("currentParticipantsAmount") as Int;
        var participantsAmount = appointment.objectForKey("participantsAmount") as Int;
        cell.participantsAmount = participantsAmount;
        var details = "\(currentParticipantsAmount)/\(participantsAmount)";
        cell.setDetails(details);
        
        var requestsAmount = appointment.objectForKey("requestsAmount") as Int;
        var guid = creator.objectForKey("guid") as String;
        var canOperationInvitation = appointment.objectForKey("canOperationInvitation") as Bool;
        var isOnwer = Utils.isOwner(guid);
        cell.showBadge(requestsAmount);
        return cell;
    }
    
    func addAcceptInvitationCallback(callback:(indexPath:NSIndexPath)->Void){
        self.acceptInvitationCallback = callback;
    }
    
    func setDetails(text:String){
        self.detailsLabel = UILabel();
        self.detailsLabel.text = text;
        self.detailsLabel.font = Utils.defaultFont();
        self.detailsLabel.textColor = UIColor.whiteColor();
        self.detailsLabel.sizeToFit();
        var originX = self.arrow.frame.origin.x - (self.detailsLabel.frame.width);
        var originY = (self.frame.height - self.detailsLabel.frame.height - 4) / 2;
        self.detailsLabel.frame.origin = CGPoint(x: originX, y: originY);
        self.contentView.addSubview(self.detailsLabel);
    }
    
    func showBadge(badgeValue:Int){
        var canOperationInvitation = self.appointment.objectForKey("canOperationInvitation") as Bool;
        if(canOperationInvitation){
            var appointmentInvitationBadge = UIButton();
            appointmentInvitationBadge.setBackgroundImage(UIImage(named: "question"), forState: UIControlState.Normal);
            appointmentInvitationBadge.sizeToFit();
            var originX = (self.arrow.frame.origin.x - appointmentInvitationBadge.frame.width) + 5;
            var originY = (self.frame.height - appointmentInvitationBadge.frame.height - 4) / 2;
            appointmentInvitationBadge.frame.origin = CGPoint(x: originX, y: originY);
            self.contentView.addSubview(appointmentInvitationBadge);
            self.detailsLabel.frame.origin.x = self.detailsLabel.frame.origin.x - appointmentInvitationBadge.frame.width;
        }
        
        var acceptedInvitation = self.appointment.objectForKey("acceptedInvitation") as Bool;
        var width:CGFloat = 10;
        var viewX = self.arrow.frame.origin.x - (width / 2);
        var viewY = (self.frame.height - width - 4) / 2;
        var frame = CGRect(x: viewX, y: viewY, width: width, height: width);
        
        if(acceptedInvitation){
            var view = UIView(frame: frame);
            view.layer.masksToBounds = true;
            view.layer.cornerRadius = 5;
            view.backgroundColor = UIColor.greenColor();
            self.contentView.addSubview(view);
            self.detailsLabel.frame.origin.x = self.detailsLabel.frame.origin.x - view.frame.width;
        }
        
        if(badgeValue > 0){
            var view = UIView(frame: frame);
            view.layer.masksToBounds = true;
            view.layer.cornerRadius = 5;
            view.backgroundColor = UIColor.redColor();
            self.contentView.addSubview(view);
            self.detailsLabel.frame.origin.x = self.detailsLabel.frame.origin.x - view.frame.width;
        }
        
        
    }
    
    func acceptInvitation(sender:UIButton){
        var url = "/api/v1/appointment/accept";
        var appointmentGuid = appointment.objectForKey("guid") as String;
        var httpBody = NSMutableDictionary();
        httpBody.setObject(appointmentGuid, forKey: "guid");
        
        HttpUtils.sendRequest(url, method: RequestMethod.POST, httpBody: httpBody, successHandle: { (response) -> Void in
            sender.removeFromSuperview();
            self.acceptInvitationCallback(indexPath: self.indexPath);
            }, errorHandle: { (error) -> Void in
        });
        
    }
}
