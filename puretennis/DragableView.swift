//
//  PersionView.swift
//  puretennis
//
//  Created by IYE Technologies on 1/15/15.
//  Copyright (c) 2015 IYE Technologies. All rights reserved.
//

import UIKit

class DragableView: NormalView {
    var dropTargetView:AppointmentScrollView?;
    
    var overlyView:UIView!;
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame);
    }
    
    init(containerView:AppointmentScrollView,dropTargetView:AppointmentScrollView?,element:NSDictionary,type:ContainerType,ownerGuid:String) {
        super.init(containerView: containerView,element: element,type: type,ownerGuid: ownerGuid);
        self.overlyView = containerView.superview;
        self.dropTargetView = dropTargetView;
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        println("touchesBegan");
        self.containerView.scrollEnabled = false;
        self.changeSuperView();
    }
    
    override func touchesMoved(touches: NSSet, withEvent event: UIEvent) {
        println("touchesMoved");
        var touch = touches.anyObject() as UITouch;
        var point = touch.locationInView(self.superview);
        self.center = point;
        println("X [\(point.x)]");
        println("Y [\(point.y)]");
    }
    
    override func touchesCancelled(touches: NSSet!, withEvent event: UIEvent!) {
        println("touchesCancelled");
        self.endDrag(touches.anyObject());
    }
    
    override func touchesEnded(touches: NSSet, withEvent event: UIEvent) {
        println("touchesEnded");
        self.endDrag(touches.anyObject());
    }
    
    func endDrag(touch:AnyObject?){
        self.containerView.scrollEnabled = true;
        var touchOffset = touch as UITouch;
        var point = touchOffset.locationInView(self.superview);
        var contains = dropTargetView!.frame.contains(point);
        println("Drop able [\(contains)]");
        if(contains){
            self.dropped();
            return;
        }
        self.changeSuperView();
        self.frame = originalFrame;
    }
    
    func changeSuperView(){
        var tmp = self.superview;
        self.removeFromSuperview();
        self.overlyView.addSubview(self);
        self.overlyView = tmp;
        var center = self.center;
        if (overlyView == containerView) {
            center.x += containerView.frame.origin.x - containerView.contentOffset.x;
            center.y += containerView.frame.origin.y - containerView.contentOffset.y;
        } else {
            center.x -= containerView.frame.origin.x - containerView.contentOffset.x;
            center.y -= containerView.frame.origin.y - containerView.contentOffset.y;
        }
        self.center = center;
    }
    
    
    func dropped(){
        self.removeFromSuperview();
        var requestGuid = element.objectForKey("guid") as String;
        var url = "/api/v1/appointment/request/accept/\(requestGuid)";
        HttpUtils.sendRequest(url, method: RequestMethod.GET, httpBody: nil, successHandle: { (response) -> Void in
            self.dropTargetView!.moveInto(self.element, type: self.type!,dropTargetView: nil);
            self.dropTargetView?.moveScrollToEndPostion();
            self.containerView.adjustSubviews();
            }, errorHandle: { (error) -> Void in
                
        });
    }
    
    func moveToTargetView(){
        self.removeFromSuperview();
        self.dropTargetView?.dropedElement(element, type: type!);
        self.dropTargetView?.moveScrollToEndPostion();
        self.containerView.adjustSubviews();
        
    }
}
