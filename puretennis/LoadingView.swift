//
//  LodingView.swift
//  puretennis
//
//  Created by IYE Technologies on 1/12/15.
//  Copyright (c) 2015 IYE Technologies. All rights reserved.
//

import UIKit

class LoadingView: UIView {

    var activityIndicatorView = UIActivityIndicatorView();
    
    var label:UILabel?;
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
    }
    
    init(superViewSize:CGSize){
        var superViewWidth = superViewSize.width;
        var superViewHeight = superViewSize.height;
        var width = superViewWidth * 0.15;
        var originX = (superViewWidth - width) / 2;
        var originY = (superViewHeight - width) / 2;
        var frame = CGRect(x: originX, y: originY, width: width, height: width);
        super.init(frame: frame);
        self.alpha = 0.7;
        self.layer.masksToBounds = true;
        self.layer.cornerRadius = width * 0.2;
        self.backgroundColor = UIColor.blackColor();
        self.initActivityIndicatorView();
    }
    
    func initActivityIndicatorView(){
        var loadingViewWidth = self.frame.width;
        var activityIndicatorViewWidth = loadingViewWidth * 0.5;
        var activityIndicatorViewOrigin = (loadingViewWidth - activityIndicatorViewWidth) / 2;
        var activityIndicatorViewFrame = CGRect(x: activityIndicatorViewOrigin, y: activityIndicatorViewOrigin, width: activityIndicatorViewWidth, height: activityIndicatorViewWidth);
        var activityIndicatorView = UIActivityIndicatorView(frame: activityIndicatorViewFrame);
        activityIndicatorView.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.WhiteLarge;
        activityIndicatorView.startAnimating();
        activityIndicatorView.hidesWhenStopped = true;
        self.addSubview(activityIndicatorView);
    }
}
