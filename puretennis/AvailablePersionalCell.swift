//
//  AvailablePersionalCell.swift
//  puretennis
//
//  Created by IYE Technologies on 12/8/14.
//  Copyright (c) 2014 IYE Technologies. All rights reserved.
//

import UIKit

class AvailablePersionalCell: AbstractPersionalCell {
   
    var partsLabel:UILabel!;
    
    var checkButton:UIButton!;
    
    var checked:Bool = false;
    
    var availability:NSDictionary!;
    
    func setup(availability:NSDictionary){
        self.availability = availability;
        var user = self.availability.objectForKey("user") as NSMutableDictionary;
        
        var userGuid = user.objectForKey("guid") as String;
        var avatar = ImageUtils.retrieveUserImage(userGuid);
        self.setAvatarImage(avatar);
        
        let name = user.objectForKey("name") as String;
        self.setName(name);
        
        self.partsLabel = UILabel();
        self.partsLabel.font = Utils.defaultFont();
        self.partsLabel.textColor = UIColor.whiteColor();
        self.partsLabel.textAlignment = NSTextAlignment.Right;
        self.contentView.addSubview(self.partsLabel);
        
        
        self.checkButton = UIButton();
        self.checkButton.setImage(UIImage(named: "unchecked"), forState: UIControlState.Normal);
        self.checkButton.addTarget(self, action: "changeCheckedStatus", forControlEvents: UIControlEvents.TouchUpInside);
        let checkButtonWidth = self.contentView.frame.height;
        let checkButtonHeight = self.contentView.frame.height;
        self.checkButton.frame.size = CGSize(width: checkButtonWidth, height: checkButtonHeight);
        
        let checkButtonX = self.frame.width - checkButton.frame.width;
        var checkButtonY = (self.frame.height - checkButton.frame.height - 4) / 2;
        self.checkButton.frame.origin = CGPoint(x: checkButtonX, y: checkButtonY);
        self.checkButton.contentMode = UIViewContentMode.Center;
        self.contentView.addSubview(checkButton);
        
        
        
        self.setParts();
    }
    
    func setParts(){
        let part = self.availability.objectForKey("date") as NSMutableDictionary;
        let partValue = self.resolvePart(part);
        
        self.partsLabel.text = partValue;
        self.partsLabel.sizeToFit();
        let labelX = self.nameLabel.frame.origin.x + self.nameLabel.frame.width + 5;
        let labelY = (self.frame.height - self.partsLabel.frame.height - 4) / 2;
        
        let labelWidth = self.frame.width - labelX - self.checkButton.frame.width;
        self.partsLabel.frame.origin = CGPoint(x: labelX, y: labelY);
        self.partsLabel.frame.size.width = labelWidth;
        
    }
    
    func resolvePart(part:NSMutableDictionary) ->String{
        var partValue = NSString();
        let parts = part.objectForKey("parts") as NSMutableArray;
        for partAsObject in parts{
            let part = partAsObject as String;
            switch part{
                case  "MORNING":
                    partValue = partValue + "上午,";
                    break;
                case "AFTERNOON":
                    partValue = partValue + "下午,";
                    break;
                case "EVENING":
                    partValue = partValue + "晚上,";
                    break;
                default :
                    break;
            }
        }
        return partValue.substringToIndex(partValue.length - 1);
    }
    
    func changeCheckedStatus(){
        self.checked = !self.checked;
        let imageNamed = self.checked ? "checked" : "unchecked";
        self.checkButton.setImage(UIImage(named: imageNamed), forState: UIControlState.Normal);
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
