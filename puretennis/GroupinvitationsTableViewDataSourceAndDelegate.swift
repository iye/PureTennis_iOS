//
//  GroupinvitationsTableViewDataSourceAndDelegate.swift
//  puretennis
//
//  Created by IYE Technologies on 11/30/14.
//  Copyright (c) 2014 IYE Technologies. All rights reserved.
//

import UIKit

class GroupinvitationsTableViewDataSourceAndDelegate: NSObject,UITableViewDataSource,UITableViewDelegate {
    
    var invitations = NSMutableArray();
    
    override init(){
        super.init();
        
        let header = NSMutableDictionary();
        header.setObject("你的好友邀请你加入下面的群", forKey: "header");
        invitations.addObject(header);
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return invitations.count;
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCellWithIdentifier("invitationCell") as UITableViewCell;
       
        let cellWidth = cell.frame.width;
        let cellHeight = cell.frame.height;
        
        if(indexPath.row == 0){
            let backgroundImage = UIImage(named: "group_invitation_header_bg");
            let backgroundImageView = UIImageView(image: backgroundImage);
            
            cell.backgroundView = backgroundImageView;
            cell.textLabel!.textAlignment = NSTextAlignment.Center;
            let header = invitations.objectAtIndex(indexPath.row) as NSMutableDictionary;
            let headerAsString = header.objectForKey("header") as String;
            cell.textLabel!.text = headerAsString;
            return cell;
        }
        
        let backgroundImage = UIImage(named: "group_cell_bg");
        let backgroundImageView = UIImageView(image: backgroundImage);
        cell.backgroundView = backgroundImageView;
        return cell;
        
    }
}
