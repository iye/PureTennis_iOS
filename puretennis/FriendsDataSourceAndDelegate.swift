//
//  FriendsTableViewDataSource.swift
//  puretennis
//
//  Created by IYE Technologies on 11/25/14.
//  Copyright (c) 2014 IYE Technologies. All rights reserved.
//

import UIkit

class FriendsDataSourceAndDelegate: NSObject,UITableViewDataSource,UITableViewDelegate {
    var friends:NSMutableArray = NSMutableArray();
    
    override init() {
        super.init();
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return friends.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCellWithIdentifier("friendCell") as UITableViewCell;
        
        let friend = friends[indexPath.row] as NSMutableDictionary;
        let friendName = friend.objectForKey("name") as String;
        return cell;
    }

    func addFriends(friends:[NSMutableDictionary]){
        self.friends.addObjectsFromArray(friends);
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true;
    }
    
    func tableView(tableView: UITableView, titleForDeleteConfirmationButtonForRowAtIndexPath indexPath: NSIndexPath) -> String! {
        return "删除";
    }
    
    // Delete friend operation is here
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
            
    }
    
}
