//
//  JoinRequestCell.swift
//  puretennis
//
//  Created by IYE Technologies on 12/2/14.
//  Copyright (c) 2014 IYE Technologies. All rights reserved.
//

import UIKit

enum RequestOperation{
    case Reject;
    case Accept;
}

class PersonalCell: AbstractPersionalCell {
    
    class func create(width:CGFloat,user:NSMutableDictionary) -> PersonalCell{
        var cell = PersonalCell();
        cell.setWidth(width);
        cell.setArrow("right_arrow_mini");
        
        var guid = user.objectForKey("guid") as String
        cell.setAvatarImage(ImageUtils.retrieveUserImage(guid))
            
        var name = user.objectForKey("name") as String
        
        var userGuid = user.objectForKey("guid") as String;
        var isCurrentUser = Utils.isCurrentUser(userGuid);
        if(isCurrentUser){
            name = "我";
        }
        cell.setName(name);
        return cell;
    }
}
