//
//  ShadeView.swift
//  puretennis
//
//  Created by IYE Technologies on 1/15/15.
//  Copyright (c) 2015 IYE Technologies. All rights reserved.
//

import UIKit

class ShadeView: UIButton {
    var chosen = false;
    var selectedImage:UIImageView?;

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame);
        self.layer.masksToBounds = true;
        self.layer.cornerRadius = 4;
    }

    func show(){
        self.hidden = false;
    }
    
    func hide(){
        self.hidden = true;
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        self.chosen = !self.chosen;
        var view = self.superview as NormalView;
        var width:CGFloat = 20;
        if(self.chosen){
            view.avatarImageView.alpha = 1;
            var frame = CGRect(x: view.avatarImageView.frame.width - width, y: view.avatarImageView.frame.height - width, width: width, height: width);
            self.selectedImage = UIImageView(frame: frame);
            self.selectedImage?.image = UIImage(named: "selected");
            self.addSubview(self.selectedImage!);
        } else {
            view.avatarImageView.alpha = 0.8;
            self.selectedImage?.removeFromSuperview();
        }
        view.chosen = self.chosen;
    }

}
