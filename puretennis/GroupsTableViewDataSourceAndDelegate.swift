//
//  GroupsTableViewDataSourceAndDelegate.swift
//  puretennis
//
//  Created by IYE Technologies on 11/30/14.
//  Copyright (c) 2014 IYE Technologies. All rights reserved.
//

import UIKit

class GroupsTableViewDataSourceAndDelegate: NSObject,UITableViewDataSource,UITableViewDelegate {
   
    
    var groups:[NSDictionary] = [];
    
    var navigationController:UINavigationController!;
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return groups.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        var group = self.groups[indexPath.row] as NSDictionary;
        var cell = GroupCell.create(tableView.frame.width,group: group);
        return cell;
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        var group = self.groups[indexPath.row] as NSDictionary;
        var groupAccess = group.objectForKey("access") as String;
        return groupAccess != "OWNER";
    }
    
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [AnyObject]? {
       var deleteAction = UITableViewRowAction(style: UITableViewRowActionStyle.Normal, title: "") { (action:UITableViewRowAction!, indexPath:NSIndexPath!) -> Void in
            self.deleteGroup(tableView, action: action, indexPath: indexPath);
        }
        deleteAction.backgroundColor = UIColor(patternImage: UIImage(named: "close_in_row")!);
        return [deleteAction];
    }
    
    func deleteGroup(tableView:UITableView,action:UITableViewRowAction!, indexPath:NSIndexPath!){
        ConfirmView(message: "确定删除此群?\n(删除后不可恢复)", confirm: { () -> Void in
            var group = self.groups[indexPath.row] as NSDictionary;
            var groupGuid = group.objectForKey("groupGuid") as String;
            var url = "/api/v1/group?groupGuid=\(groupGuid)";
            HttpUtils.sendRequest(url, method: RequestMethod.DELETE, httpBody: nil, successHandle: { (response) -> Void in
                self.groups.removeAtIndex(indexPath.row);
                tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic);
                }, errorHandle: { (error) -> Void in
                    
            });
            }, cancel: nil).show();
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    }
    
    // Toggle to details view is here
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var group = self.groups[indexPath.row] as NSDictionary;
        self.toggleView(group)
        
    }
    
    func toggleView(group:NSDictionary){
        var isOwner = group.objectForKey("owner") as Bool;
        
        var storyboard = UIStoryboard(name: "GroupDetails", bundle: nil);
        
        var access = group.objectForKey("access") as String;
        
        if(access == "OWNER"){
            storyboard = UIStoryboard(name: "Main", bundle: nil);
            var friendsViewController = storyboard.instantiateViewControllerWithIdentifier("friendsView") as FriendsViewController;
            self.navigationController.pushViewController(friendsViewController, animated: true);
            return;
        }
        var groupGuid = group.objectForKey("groupGuid") as String;
        var groupName = group.objectForKey("groupName") as String;
        var group = NSMutableDictionary();
        group.setObject(groupGuid, forKey: "guid");
        group.setObject(groupName, forKey: "name");
        if(isOwner){
            var groupDetailsAsOwnerView = storyboard.instantiateViewControllerWithIdentifier("groupDetailsAsOwnerView") as GroupDetailsAsOwnerViewController;
            groupDetailsAsOwnerView.setup(group);
            self.navigationController?.pushViewController(groupDetailsAsOwnerView, animated: true);
            return;
        }
        var groupDetailsAsParticipantView = storyboard.instantiateViewControllerWithIdentifier("groupDetailsAsParticipantView") as GroupDetailsAsParticipantViewController;
        groupDetailsAsParticipantView.setup(group);
        self.navigationController?.pushViewController(groupDetailsAsParticipantView, animated: true);
    }
 
    
    func navigationController(navigationController:UINavigationController){
        self.navigationController = navigationController;
    }
    
    
}
