//
//  RegisterViewController.swift
//  puretennis
//
//  Created by IYE Technologies on 1/9/15.
//  Copyright (c) 2015 IYE Technologies. All rights reserved.
//

import UIKit

class RegisterViewController: ImagePickerController {

    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var usernameTextField: CustomTextField!
    @IBOutlet weak var passwordTextField: CustomTextField!
    @IBOutlet weak var femaleButton: GenderButton!
    @IBOutlet weak var maleButton: GenderButton!
    
    var gender = "MALE";
    var adjustContentToAccommodateKeyboard:AdjustContentToAccommodateKeyboard!;
    override func viewDidLoad() {
        super.viewDidLoad()
        ViewUtils.addBackgroundView(self);
        self.initAvatarImageView();
        self.adjustContentToAccommodateKeyboard = AdjustContentToAccommodateKeyboard(target: self);
    }
    
    func initAvatarImageView(){
        self.avatarImageView.layer.masksToBounds = true;
        self.avatarImageView.layer.cornerRadius = 8;
    }
    
    func selecedIdol(idolPicker:IdolPicker){
        var idol = idolPicker.selecedIdol;
        var idolName = idol.objectForKey("name") as String;
    }

    //Choice avatar
    @IBAction func choiceAvatarTouchUp(sender: UIButton) {
        self.pickingImage();
    }
    
    override func didFinishPickingImage(image: UIImage!) {
        self.avatarImageView.image = image;
    }
    
    @IBAction func checkMale(sender: UIButton) {
        if(self.gender != "MALE"){
            self.gender = "MALE";
            sender.setImage(UIImage(named: "male_checked"), forState: UIControlState.Normal);
            self.femaleButton.setImage(UIImage(named: "female_unchecked"), forState: UIControlState.Normal);
        }
    }
    
    @IBAction func checkFemale(sender: UIButton) {
        if(self.gender != "FEMALE"){
            self.gender = "FEMALE";
            sender.setImage(UIImage(named: "female_checked"), forState: UIControlState.Normal);
            self.maleButton.setImage(UIImage(named: "male_unchecked"), forState: UIControlState.Normal);
        }
        
    }

    @IBAction func back(sender:UIButton){
        self.dismissViewControllerAnimated(true, completion: nil);
    }
    
    @IBAction func createAccount(sender: UIButton) {
        if(validate()){
            self.processing();
            var url = "/api/v1/user/register";
            var username = self.usernameTextField.text;
            var password = self.passwordTextField.text;
            var imageBase64Byte = avatarBase64();
            var registerBody = NSMutableDictionary();
            registerBody.setObject(username, forKey: "nickname");
            registerBody.setObject(password, forKey: "password");
            registerBody.setObject(self.gender, forKey: "gender");
            registerBody.setObject("", forKey: "idolGuid");
            registerBody.setObject(imageBase64Byte, forKey: "imageBase64Byte");
            var data: AnyObject? = HttpUtils.sendSynchronousRequest(url, method: RequestMethod.POST, requestBody: registerBody);
            if(data != nil){
                self.processComplete();
                var dictionary = data as NSDictionary;
                var guid = dictionary.objectForKey("guid") as String;
                var token = dictionary.objectForKey("token") as String;
                Utils.saveData(guid, key: "guid");
                Utils.saveData(token, key: "token");
                var viewController = AuthorisationService.mainView();
                self.presentViewController(viewController, animated: true, completion: nil);
            }
        }
    }
    
    func avatarBase64() -> String{
        var avatarImage = self.avatarImageView.image;
        if(avatarImage != nil){
            return ImageUtils.base64String(avatarImage!);
        }
        return "";
    }
    
    func validate() -> Bool{
        var username = self.usernameTextField.text;
        var password = self.passwordTextField.text;
        var unique = true;
        if(username.isEmpty){
            self.usernameTextField.shake();
        } else {
            var url = "/api/v1/user/checkunique";
            var checkUsername = NSMutableDictionary();
            checkUsername.setObject(username, forKey: "nickname");
            var data: AnyObject? = HttpUtils.sendSynchronousRequest(url, method: RequestMethod.POST, requestBody: checkUsername);
            if(data != nil){
                var uniqueAsString = data as NSString;
                unique = (uniqueAsString.lowercaseString == "true");
                if(!unique){
                    self.usernameTextField.shake();
                }
                Utils.debug("Username [\(username)] unique is [\(unique)]");
            }
        }
        if(password.isEmpty){
            self.passwordTextField.shake();
        }
        return (!username.isEmpty && !password.isEmpty && unique);
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
