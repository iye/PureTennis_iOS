//
//  AuthorisationService.swift
//  puretennis
//
//  Created by IYE Technologies on 11/13/14.
//  Copyright (c) 2014 IYE Technologies. All rights reserved.
//

import UIKit
class AuthorisationService:NSObject,TencentLoginDelegate,TencentSessionDelegate{
    
    func tencentDidLogin() {
        
    }
    
    func tencentDidNotLogin(cancelled: Bool) {
        
    }
    
    func tencentDidNotNetWork() {
        
    }
    
    func tencentNeedPerformReAuth(tencentOAuth: TencentOAuth!) -> Bool {
        
        return false;
    }
    
    
    class func registerDevice(){
        
        let token: AnyObject? = Utils.getValue("token");
        if(token != nil){
            
            let deviceToken: AnyObject? = Utils.getValue("deviceToken")
            
            if(deviceToken != nil){
                var url = "/api/v1/user/adddevice"
                
                var body = NSMutableDictionary();
                body.setObject(deviceToken!, forKey: "deviceToken");
                body.setObject("apple", forKey: "type");
                HttpUtils.sendRequest(url, method: RequestMethod.POST, httpBody: body, successHandle: { (response) -> Void in
                    
                    }, errorHandle: { (error) -> Void in
                        
                })
                
            }
            
        }
    }
        
    class func mainView() -> UIViewController {
        let token: AnyObject? = Utils.getValue("token");
        
        var storyboard:UIStoryboard;
        if(token != nil){
            var puretennisTabBarController = PureTennisTabBarController();
            return puretennisTabBarController;
        }
        
        storyboard = UIStoryboard(name: "Login", bundle: nil);
        let loginViewController = storyboard.instantiateViewControllerWithIdentifier("loginView") as LoginViewController
        
        return loginViewController;
        
    }
    
    class func logout(){
        let userDefaults = NSUserDefaults();
        var token = Utils.getValue("token") as String
        var deviceToken: AnyObject? = Utils.getValue("deviceToken")
        var deviceTokenString = deviceToken == nil ? "empty": (deviceToken as String)
        var body = NSMutableDictionary()
        body.setValue(token, forKey: "token")
        body.setValue(deviceTokenString, forKey: "deviceToken")
        body.setValue("apple", forKey: "deviceType")
        var url = "/api/v1/logout"
        HttpUtils.sendRequest(url, method: RequestMethod.POST, httpBody: body, successHandle: { (response) -> Void in
            Utils.removeData("token")
        }, errorHandle: { (error) -> Void in
            
        })
        
    }
}