//
//  Property.swift
//  puretennis
//
//  Created by IYE Technologies on 12/5/14.
//  Copyright (c) 2014 IYE Technologies. All rights reserved.
//

import Foundation
import UIKit

struct Properties {
    
//    static var WEBSOKET_HOST = "115.28.107.1"
    
    static var WEBSOKET_HOST = "192.168.1.146:8080"
    
    static var HOST = "http://\(Properties.WEBSOKET_HOST)";
    
    
    
    static let DEFAULT_USERNAME = "德约科维奇";
    
    static let DEFAULT_PASSWORD = "123456";
    
    static let ITEMS_PER_PAGE = 50;
    
    static let DEFAULT_FONT_SIZE:CGFloat = 16;
    
    static let enableCache = false;
    
    static let TENCENT_APPID = "1103842030";
}

