//
//  ImagePickerControllerViewController.swift
//  puretennis
//
//  Created by IYE Technologies on 12/26/14.
//  Copyright (c) 2014 IYE Technologies. All rights reserved.
//

import UIKit

class ImagePickerController: UIViewController,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    var selectedImage:UIImage?;
    
    private var actionSheel:UIActionSheet!;
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func pickingImage(){
        self.actionSheel = UIActionSheet(title: "设置头像", delegate: self, cancelButtonTitle: "取消", destructiveButtonTitle: nil);
        self.actionSheel.addButtonWithTitle("拍照");
        self.actionSheel.addButtonWithTitle("从相册获取");
        self.actionSheel.showInView(self.view);
    }
    
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int) {
        switch buttonIndex{
        case 1:
            // 打开相机
            self.openCamera();
            break;
        case 2:
            // 打开相册
            self.openAlbums();
            break;
        default:
            break;
        }
    }
    
    // Open camera
    private func openCamera(){
        let sourceType = UIImagePickerControllerSourceType.Camera;
        let isCameraAvailable = UIImagePickerController.isSourceTypeAvailable(sourceType);
        
        if(isCameraAvailable){
            let imatePicker = UIImagePickerController();
            imatePicker.sourceType = sourceType;
            imatePicker.delegate = self;
            imatePicker.allowsEditing = true;
            
            self.presentViewController(imatePicker, animated: true, completion: nil);
        }
    }
    
    // Open albums
    private func openAlbums(){
        let imagePicker = UIImagePickerController();
        imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary;
        imagePicker.delegate = self;
        imagePicker.allowsEditing = true;
        self.presentViewController(imagePicker, animated: true, completion: nil);
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        picker.dismissViewControllerAnimated(true, completion: nil);
    }

    func imagePickerController(picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!) {
        Utils.debug("Image size: \(image.size)")
        var resized = ImageUtils.resizeImage(image, maxSize: 160);
        
        Utils.debug("Resized to \(resized.size)")

        self.selectedImage = image;
        self.upload(resized);
        self.didFinishPickingImage(resized);
        picker.dismissViewControllerAnimated(true, completion: nil);
    }

    // Can be override this method in sub class
    func upload(image:UIImage!){
        var avatarBase64 = Utils.base64String(image);
        var currentUserGuid = Utils.currentUserGuid();
        Utils.saveData(avatarBase64, key: "image_\(currentUserGuid)");
        Utils.debug("Cached image successfully");
        let url = "/api/v1/profile/image";
        let postData = NSMutableDictionary();
        postData.setObject(avatarBase64, forKey: "base64Bytes");
        HttpUtils.sendRequest(url, method: RequestMethod.POST, httpBody: postData, successHandle: { (response) -> Void in
            Utils.debug("Upload avatar successfully");
            }, errorHandle: nil);
    }
    
    // Can be override this method in sub class
    func didFinishPickingImage(image:UIImage!){
        fatalError("didFinishPickingImage(image:UIImage!) has not been implemented");
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning();
        Utils.debug("Memory warning")
    }
    
    
}
