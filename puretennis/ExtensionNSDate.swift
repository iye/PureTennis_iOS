//
//  ExtensionNSDate.swift
//  puretennis
//
//  Created by IYE Technologies on 12/26/14.
//  Copyright (c) 2014 IYE Technologies. All rights reserved.
//

import UIKit

extension NSDate {
   
    func getYear() ->Int{
        var components = NSCalendar.currentCalendar().components(NSCalendarUnit.CalendarUnitYear, fromDate: self);
        return components.year;
    }
    
    func getMonth() ->Int{
        var components = NSCalendar.currentCalendar().components(NSCalendarUnit.CalendarUnitMonth, fromDate: self);
        return components.month;
    }
    
}
