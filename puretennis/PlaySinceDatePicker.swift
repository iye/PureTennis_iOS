//
//  PlaySinceDatePicker.swift
//  puretennis
//
//  Created by IYE Technologies on 12/25/14.
//  Copyright (c) 2014 IYE Technologies. All rights reserved.
//

import UIKit

class PlaySinceDatePicker: AbstractPickerView {
    var datePicker = UIPickerView();
    var years = NSMutableArray();
    var months = NSMutableArray();
    
    var year:Int!;
    var month:Int!;
    var valueAsText:String!;
    
    var confirmCallback:((response:AnyObject?)->Void)?;
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame);
        var currentYear = NSDate().getYear();
        for(var year = currentYear; year >= currentYear - 100; year--){
            self.years.addObject(year);
        }
        for(var month = 1; month <= 12; month++){
            self.months.addObject(month);
        }
        
        self.datePicker.dataSource = self;
        self.datePicker.delegate = self;
        var margin:CGFloat = 80;
        self.datePicker.frame.origin = CGPoint(x: margin, y: 0);
        self.datePicker.frame.size.width = self.showFrame.size.width - margin * 2;
        self.contentView.addSubview(datePicker);
        
        self.year = self.years.firstObject as Int;
        self.month = self.months.firstObject as Int;
        self.valueAsText = "\(self.year)-\(self.month)";
    }
    
    func addConfirmCallback(callback:((response:AnyObject?)->Void)){
        self.confirmCallback = callback;
    }
    
    override func confirm(sender:UIButton){
        Utils.debug("Play since [\(self.valueAsText)]");
        var url = "/api/v1/user/updateplaysince";
        var playSince = NSMutableDictionary();
        playSince.setObject(valueAsText, forKey: "playSince");
        HttpUtils.sendRequest(url, method: RequestMethod.POST, httpBody: playSince, successHandle: { (response) -> Void in
            self.confirmCallback!(response: response);
            self.hide();
        }, errorHandle: { (error) -> Void in
            
        });
    }
    
    // returns the number of 'columns' to display.
    override func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int{
        return 2;
    }
    
    // returns the # of rows in each component..
    override func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        if(component == 0){
            return years.count;
        }
        return months.count;
    }
    
    func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView!) -> UIView {
        var pickerLabel = UILabel();
        pickerLabel.textAlignment = NSTextAlignment.Center;
        if(component == 0){
            var year = self.years.objectAtIndex(row) as Int;
            pickerLabel.text = "\(year)年";
            pickerLabel.sizeToFit();
            return pickerLabel;
        } else {
            var month = self.months.objectAtIndex(row) as Int;
            pickerLabel.text = "\(month)月";
            var currentYear = getCurrentYear();
            var currentMonth = getCurrentMonth();
            if(self.year == currentYear && month > currentMonth){
                pickerLabel.textColor = UIColor.lightGrayColor();
            }
            pickerLabel.sizeToFit();
            return pickerLabel;
        }
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if(component == 0){
            self.year = self.years.objectAtIndex(row) as Int;
        } else {
            self.month = self.months.objectAtIndex(row) as Int;
        }
        
        pickerView.reloadComponent(1);
        
        var currentYear = getCurrentYear();
        var currentMonth = getCurrentMonth();
        if(self.year == currentYear && self.month > currentMonth){
            self.month = currentMonth;
            self.datePicker.selectRow((currentMonth - 1), inComponent: 1, animated: true);
        }
        self.valueAsText = "\(self.year)-\(self.month)";
        self.sendActionsForControlEvents(UIControlEvents.ValueChanged);
    }
    
    func selectDate(year:Int,month:Int){
        self.year = year;
        self.month = month;
        self.valueAsText = "\(self.year)-\(self.month)";
        var yearIndex = self.years.indexOfObject(year);
        self.datePicker.selectRow(yearIndex, inComponent: 0, animated: false);
        var monthIndex = self.months.indexOfObject(month);
        self.datePicker.selectRow(monthIndex, inComponent: 1, animated: false);
    }
    
    func getCurrentYear() -> Int{
        var now = NSDate();
        var yearAsString = DateUtils.toString(now, format: "yyyy");
        return yearAsString.toInt()!;
    }
    
    func getCurrentMonth() -> Int{
        var now = NSDate();
        var monthAsString = DateUtils.toString(now, format: "MM");
        return monthAsString.toInt()!;
    }
}
