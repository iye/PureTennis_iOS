//
//  AppointmentTimePicker.swift
//  puretennis
//
//  Created by IYE Technologies on 1/6/15.
//  Copyright (c) 2015 IYE Technologies. All rights reserved.
//

import UIKit

class AppointmentTimePicker: UIPickerView,UIPickerViewDataSource,UIPickerViewDelegate {

    let minuteInterval:Int = 15;
    
    var hours:[Int] = [];
    var minutes:[Int] = [];
    
    var hour:Int!{
        didSet{
            self.selectRow(self.hour, inComponent: 0, animated: false);
            self.reloadComponent(0);
            self.value = "\(self.hour):\(self.minute)";
        }
    };
    var minute:Int!{
        didSet{
            self.selectRow((self.minute / minuteInterval), inComponent: 1, animated: false);
            self.reloadComponent(1);
            self.value = "\(self.hour):\(self.minute)";
        }
    };
    var value:String!;
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
    }
    
    override init() {
        super.init();
        self.frame.size.width = 60;
        self.dataSource = self;
        self.delegate = self;
        self.initHours();
        self.initMinutes();
        self.defaultSelect();
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame);
    }
    
    func initHours(){
        for(var hour = 0; hour < 24; hour++){
            self.hours.append(hour);
        }
        self.hour = DateUtils.getCurrentHour();
    }
    
    func initMinutes(){
        self.minutes = [0,15,30,45];
    }
    
    func defaultSelect(){
        var currentMinute = DateUtils.getCurrentMinute();
        if(currentMinute % minuteInterval == 0){
            self.minute = currentMinute;
        } else {
            self.minute = ((currentMinute / minuteInterval) + 1) * minuteInterval;
        }
        
        if(currentMinute >= self.minutes.last){
            self.hour = self.hour + 1;
            self.minute = self.minutes.first;
        } else {
            self.hour = DateUtils.getCurrentHour();
        }
        
        self.value = "\(self.hour):\(self.minute)";
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int{
        return 2;
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        if(component == 0){
            return self.hours.count;
        }
        return self.minutes.count;
    }
    
    func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView!) -> UIView {
        var label = UILabel();
        if(component == 0){
            var currentHoure = DateUtils.getCurrentHour();
            var currentMinute = DateUtils.getCurrentMinute();
            var hour = self.hours[row];
            label.text = (hour < 10) ? "0\(hour)" : "\(hour)";
            label.sizeToFit();
            return label;
        }
        var currentMinute = DateUtils.getCurrentMinute();
        var minute = self.minutes[row];
        label.text = (minute < 10) ? "0\(minute)" : "\(minute)";
        label.sizeToFit();
        return label;
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if(component == 0){
            self.hour = self.hours[row];
        }
        if(component == 1){
            self.minute = self.minutes[row];
        }
        
        var hourAsString = self.hour < 10 ? "0\(self.hour)" : "\(self.hour)";
        var minuteAsString = self.minute < 10 ? "0\(self.minute)" : "\(self.minute)";
        self.value = "\(hourAsString):\(minuteAsString)";
    }
    
    func pickerView(pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 35;
    }
}
