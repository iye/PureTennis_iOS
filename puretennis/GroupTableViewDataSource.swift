//
//  GroupTableViewDataSource.swift
//  puretennis
//
//  Created by IYE Technologies on 11/23/14.
//  Copyright (c) 2014 IYE Technologies. All rights reserved.
//

import UIKit

class GroupTableViewDataSource: UITableViewDataSource {

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return groups.count;
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        
        let group = self.groups.objectAtIndex(indexPath.row) as NSDictionary;
        let groupName = group.objectForKey("groupName") as String;
        
        if(groupName == "loadMore"){
            var loadMore = groupTableView.dequeueReusableCellWithIdentifier("loadMoreCell") as UITableViewCell;
            return loadMore;
        }
        
        var cell = tableView.dequeueReusableCellWithIdentifier("groupCell") as UITableViewCell;
        
        cell.textLabel.text = groupName;
        
        return cell;
    }
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
