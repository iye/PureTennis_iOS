//
//  puretennisTests.swift
//  puretennisTests
//
//  Created by IYE Technologies on 11/9/14.
//  Copyright (c) 2014 IYE Technologies. All rights reserved.
//

import UIKit
import XCTest

class PuretennisTest: XCTestCase {
    override func setUp() {
        super.setUp()
        Utils.debug("\n----- Output Start --------------------------------------\n");
    }
    
    
    func loopRun(){
        CFRunLoopRun();
    }
    
    func stopLoopRun(){
        let currentLoop = CFRunLoopGetCurrent();
        CFRunLoopStop(currentLoop);
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        Utils.debug("\n----- Output End --------------------------------------\n\n");
        
    }
}
