//
//  UtilsTest.swift
//  puretennis
//
//  Created by IYE Technologies on 11/20/14.
//  Copyright (c) 2014 IYE Technologies. All rights reserved.
//

import UIKit
import XCTest
import Foundation

/*
*
*如果测试的方法是异步方法,在调用异步方法后 用CFRunLoopRun()阻塞线程.在异步方法的回调方法里停止阻塞.
*
*/

class UtilsTest: PuretennisTest {
 
    func testSendGetRequest(){
        
        let url = "http://192.168.2.105:8080/api/v1/user/availabilities/2014-11-11/2014-11-16";
        HttpUtils.sendRequest(url, method: RequestMethod.GET, httpBody: nil, successHandle: { (response) -> Void in
            
            Utils.debug("Request success response [\(response)]");
            
            self.stopLoopRun();
            }, errorHandle: { (error:NSError?) -> Void in
                Utils.debug("Error [\(error)]");
                
                self.stopLoopRun();
        });
        

        loopRun();
    }
    
    
    func testSendPostRequest(){
        var jsonString = "{\"date\":\"2014-11-11\",\"period\":\"MORNING\"}"
        
        var postDictionary = NSMutableDictionary();
        
        postDictionary.setObject("2014-11-11", forKey: "date");
        
        postDictionary.setObject("MORNING", forKey: "period");
        
        HttpUtils.sendRequest("http://192.168.2.105:8080/api/v1/user/availability", method: RequestMethod.POST, httpBody: postDictionary, successHandle: { (response) -> Void in
            
            Utils.debug("Request success response [\(response)]");
            
            self.stopLoopRun();
            
            }, errorHandle: { (error:NSError?) -> Void in
                Utils.debug("Error [\(error)]");
                self.stopLoopRun();
        });
        
        loopRun();
    
    }
    
    
    func testPList(){
        
        // Root dictionary
        var rootDictionary = NSMutableDictionary();


        var dictionaries:NSMutableArray = NSMutableArray();
        
        var dictionaryFist = NSMutableDictionary();
        dictionaryFist.setObject("你好", forKey: "nameAsChinase");
        dictionaryFist.setObject("hello", forKey: "nameAsEnglish");
    
        dictionaries.addObject(dictionaryFist);
        
        var dictionarySecond = NSMutableDictionary();
        dictionarySecond.setObject("AAAA", forKey: "A");
        dictionarySecond.setObject("BBBB", forKey: "B");
        dictionarySecond.setObject("CCCC", forKey: "C");
        
        dictionaries.addObject(dictionarySecond);

        
        rootDictionary.setObject(dictionaries, forKey: "dictionaries");
        
        // Get document directory
        let pathArray:NSArray =  NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true);
     
        let path = pathArray.objectAtIndex(0) as String;
        
        
        // if plist file not exist will auto create it
        let filePath = path.stringByAppendingPathComponent("puretennis.plist");
        
        rootDictionary.writeToFile(filePath, atomically: true);
        
        
        let readRootDictionary = NSMutableDictionary(contentsOfFile: filePath);
        
        let readArray = readRootDictionary?.objectForKey("dictionaries") as NSMutableArray;
        
        // Read dictionaries array
        Utils.debug("Read array [\(readArray)]");
        
        // Add new data
        rootDictionary.setObject(NSMutableDictionary(), forKey: "emptyDictionary");
        
        // Write new root Dictionary
        rootDictionary.writeToFile(filePath, atomically: true);
        
        
        let secondReadRootDictionary = NSMutableDictionary(contentsOfFile: filePath);
        
        
        // Second read dictionaries array
        let secondReadDictionaryArray = secondReadRootDictionary?.objectForKey("dictionaries") as NSMutableArray;
        Utils.debug("Second read dictionaries array [\(secondReadDictionaryArray)]");
        
        let reademptyDictionary = secondReadRootDictionary?.objectForKey("emptyDictionary") as NSMutableDictionary;
        
        Utils.debug("Empty dictionary [\(reademptyDictionary)]");
        
        let secondFilepath = path.stringByAppendingPathComponent("test.plist");
        reademptyDictionary.writeToFile(secondFilepath, atomically: true);
        
        Utils.debug("secondFilepath [\(secondFilepath)]");

        
        let dicrectory:NSArray =  NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true);
        

        Utils.debug("dicrectory [\(dicrectory)]");
        
        
    }
    
    
    func testImageConvertToBase64(){
        var image = UIImage(named: "avatar.jpg");
        
        Utils.debug("Origin image [\(image)]");
        let imageData = UIImagePNGRepresentation(image);
        
        let base64 = imageData.base64EncodedStringWithOptions( NSDataBase64EncodingOptions.Encoding64CharacterLineLength);
        Utils.debug("Base64 [\(base64)]");
        
        
        let decodedData = NSData(base64EncodedString: base64, options: NSDataBase64DecodingOptions.IgnoreUnknownCharacters);
        
        image = UIImage(data: decodedData!);
        Utils.debug("decoded image [\(image)]");
        
    
    }
    
    func testFont(){
    let huawenxingkai = UIFont(name: "huawenhupo", size: 12);
        for familyName in UIFont.familyNames(){
            
            let fontFamilyName = familyName as String;
            Utils.debug();
            Utils.debug("familyName [\(fontFamilyName)]");
            Utils.debug();
            for font in UIFont.fontNamesForFamilyName(fontFamilyName){
                Utils.debug("font [\(font)]");
            }
            Utils.debug();
        }
        
    }
    
    
    func testProperties(){
        Utils.debug("Properties.HOST [\(Properties.HOST)]");
        
        Properties.HOST = "http://pt.iye.hk";
        
        Utils.debug("Properties.HOST [\(Properties.HOST)]");
    }
    
    func testDayOfCurrentWeek(){
        var week = DateUtils.firstDayOfCurrentWeek();
        Utils.debug(week);
    }
    
    func testDateInterval(){
        let from = DateUtils.parseDate("2014-12-03 19:23:50", format: "yyyy-MM-dd HH:mm:ss");
        
        let to = DateUtils.parseDate("2014-12-03 19:23:52", format: "yyyy-MM-dd HH:mm:ss");
        
        let second = DateUtils.timeInterval(from, to: to);
        
        Utils.debug("[\(from) to [\(to)] is interval [\(second)] second");
        
    }
    
    
    func testCompareDate(){
        var dateAsString = "2014-12-11";
        
        var isEqual = DateUtils.isDateInToday(dateAsString, fomat: "yyyy-MM-dd");
        Utils.debug("Is equal [\(isEqual)]");
    
        
       var currentCalendar = NSCalendar.currentCalendar();
        var isDateInToday = currentCalendar.isDateInToday(DateUtils.parseDate(dateAsString, format: "yyyy-MM-dd"));
         Utils.debug("Is Date In Today [\(isDateInToday)]");
        
        
        
        var equal = currentCalendar.compareDate(NSDate(), toDate: NSDate(), toUnitGranularity: NSCalendarUnit.CalendarUnitDay);
        Utils.debug("CompareDate [\(equal == NSComparisonResult.OrderedSame)]");
        
        
        var now = NSDate();
        
        var todayAsCN = DateUtils.dateResolve(now);
        Utils.debug("Date is today [\(todayAsCN)]");
       
        var tomorrowAsCN = DateUtils.dateResolve(DateUtils.plusDays(now, days: 1));
        Utils.debug("Date is tomorrow [\(tomorrowAsCN)]");
        
        var unresolveDate = DateUtils.dateResolve(DateUtils.plusDays(now, days: 2));
        Utils.debug("Can not resolve date. return self [\(unresolveDate)]");
        
    }
    
    func testDateFormat(){
        var dateAsString = "2014年12月12日";
        var date = DateUtils.parseDate(dateAsString, format: "yyyy年MM月dd日");
        Utils.debug("Date [\(date)]");
    }
    
    
    
}
